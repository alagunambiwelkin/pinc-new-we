class AnswersController < ApplicationController
  before_filter :load_resource, :only => [:update, :destroy, :show]
  before_filter :ensure_correct_user, :only => [:update, :destroy]
  before_filter :load_question, :only => [:create]
  skip_before_filter :load_user_with_api_key, :only => [:show]

  def create
    @answer = @user.answers.build(params[:answer]) { |answer| answer.question = @question }
    if @answer.save
      render :json => { :message => "Answer posted successfully" }
    else
      render :json => { :message => "Answer cannot be saved. Please try again."}
    end
  end

  def destroy
    if @answer.destroy
      render :json => { :message => "Answer destroyed successfully" }
    else
      render :json => { :message => "Answer cannot be destroyed. Please try again." }
    end
  end

  def update
    if @answer.update_attributes(params[:answer])
      render :json => { :message => "Answer has been updated successfully" }
    else
      render :json => { :message => "Answer cannot be updated" }
    end
  end

  def show
    render :json => @answer.to_hash
  end

  private

    def load_question
      @question = Question.where(:id => params[:question_id]).first
      render :json => { :message => "No question found" } unless @question
    end

end
