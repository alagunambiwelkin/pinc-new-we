class ApnDevicesController < ApplicationController
	skip_before_filter :load_user_with_api_key

	def reset_device_badge_count
		device = APN::Device.where(:token => params[:device_token]).first
		if device && device.update_attribute('badge_count', 0)
			render :json => {:message => "Badge count reset" }, :status => :ok
		else
			render :json => {:message => "Cannot update." }, :status => :ok
		end
	end
end
