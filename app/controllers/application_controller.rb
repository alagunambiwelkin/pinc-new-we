class ApplicationController < ActionController::Base
  respond_to :json
  before_filter :load_user_with_api_key

  private
    def objects_hash(objects, is_review_collection = false)
      # NOTE AK - use if
      unless is_review_collection
        objects.collect { |object| object.to_hash } 
      else
        # TODO -> Use scope with_user in vote model and we can use present? here
        objects.collect { |object| object.to_hash("already_voted" => (object.votes.where('user_id = ?', @user.id).count(:id) > 0)) }
      end
    end

    def load_user_with_api_key
      unless request.env['HTTP_AUTHORIZATION'] && @user = User.with_api_key(request.env['HTTP_AUTHORIZATION'])
        render :json => { "message" => "User Not Found" }, :status => :not_found
      end
    end

    def load_resource(includes = [])
      @object = model_class.includes(includes).where(:id => params[:id]).first
      if @object
        instance_variable_set("@#{controller_name.singularize}", @object)
      else
        render :json => {"message" => "#{model_class} Not Found"}, :status => :not_found            
      end
    end

    def get_instance_object
      instance_variable_get("@#{controller_name.singularize}")
    end

    def model_class
      "#{controller_name.classify}".constantize
    end

    def ensure_correct_user
      @object = get_instance_object
      unless @object.user == @user
        render :json => {:message => "You Donot Own this #{model_class}"}, :status => :unauthorized
      end
    end

    def generate_and_add_new_keys(object, object_collection, key)
      if object.is_a?(ActiveRecord::Relation)
        services_collection = []
        object.each do |service|
          services_collection << service.to_hash(key => (object_collection.include?(service.id)))
        end
        services_collection
      else
        object.to_single_hash(key => (object_collection.include?(object.id)), :type => object.type)
      end
    end

    def load_user_if_api_key_is_sent(includes=[])
      @user = User.includes(includes).with_api_key(request.env['HTTP_AUTHORIZATION']) if request.env['HTTP_AUTHORIZATION']
    end

    def set_response_header(object)
      @images = object.return_all_images_of_product_including_review_images
      no_of_pages = (@images.count.to_f / IMAGES_PER_PAGE).ceil
      response.headers['no_of_pages'] = no_of_pages.to_s
    end

    def set_response_header_for_search
      object_collection = instance_variable_get("@#{controller_name}")
      no_of_pages = (object_collection.count.to_f / RESULTS_PER_PAGE).ceil
      response.headers['no_of_pages'] = no_of_pages.to_s
    end
end
