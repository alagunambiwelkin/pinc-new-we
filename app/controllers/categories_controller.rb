class CategoriesController < ApplicationController
  skip_before_filter :load_user_with_api_key, :unless => lambda {|c| c.params[:type] == "events" }

  def categories
    categories = Category.where(:product_category_type => params[:type].classify)
    render :json => categories, :status => :ok
  end
end
