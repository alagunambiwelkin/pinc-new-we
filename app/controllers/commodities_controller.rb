class CommoditiesController < ApplicationController
  skip_before_filter :load_user_with_api_key#, :only => [:show, :search]
  before_filter :load_user_if_api_key_is_sent, :only => [:search, :show]
  before_filter :load_resource, :only => [:show, :update, :destroy]
  before_filter :ensure_correct_user, :only => [:update, :destroy]
  before_filter :load_search_results, :only => [:search]
  before_filter :set_response_header_for_search, :only => [:search]

  def index
    render :json => objects_hash(@user.created_commodities), :status => :ok
  end

  # DONE -> This action uses in three locations to get categories. We can move this action to categories_controller to get categories with different product_category_type.

  def create
    commodity = Commodity.new(params[:commodity])
    # commodity = @user.created_commodities.build(params[:commodity])
    if commodity.save
      render :json => { :message => "Item has been listed successfully and would be available shortly", :id => commodity.id }, :status => :ok
    else
      render :json => { :message => commodity.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
    end
  end

  def update
    if @commodity.update_attributes(params[:commodity])
      render :json => {}, :status => :ok
    else
      render :json => {:message => @commodity.errors.full_messages.join(";\n")}, :status => :unprocessable_entity
    end
  end

  def show
    if @user
      render :json => @commodity.to_single_hash(:is_favorite => @user.favorite?(@commodity.id), :type => @commodity.type), :status => :ok
    else
      render :json => @commodity.to_single_hash(:type => @commodity.type), :status => :ok
    end
  end

  def destroy
    if @commodity.destroy
      message, status = "Item Destroyed Successfully", :ok 
    else
      message, status = @commodity.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => {:message => message}, :status => status
  end


  def search
    if user_favorite_commodities = @user.try(:favorite_product_ids)
      render :json => generate_and_add_new_keys(@commodities.page(params[:page]).per(RESULTS_PER_PAGE), user_favorite_commodities, :is_favorite), :status => :ok
    else
      render :json => objects_hash(@commodities.page(params[:page]).per(RESULTS_PER_PAGE)), :status => :ok
    end
  end

  private

  def load_search_results
    @commodities = Commodity.search_with(params[:name])
  end
end
