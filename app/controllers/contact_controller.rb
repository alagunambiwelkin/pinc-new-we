class ContactController < ApplicationController
  skip_before_filter :load_user_with_api_key

  def contact_us
    Contact.delay.send_contact_mail(params[:from], params[:subject], params[:body])
    render :json => {"message" => "Successfully Sent Contact Mail..."}
  end
end