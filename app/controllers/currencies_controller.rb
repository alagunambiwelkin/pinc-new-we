class CurrenciesController < ApplicationController
  skip_before_filter :load_user_with_api_key

  def index
    render :json => objects_hash(Currency.scoped), :status => :ok
  end
end
