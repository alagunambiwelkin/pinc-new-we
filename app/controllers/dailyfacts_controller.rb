require 'date.rb'
class DailyfactsController < ApplicationController
  skip_before_filter :load_user_with_api_key
  # GET /dailyfacts
  # GET /dailyfacts.json
  def index
    if ((Dailyfact.all.length > 0) && (Currentfact.all.length > 0) && (@current = Currentfact.all.first) && (@currentfact = Dailyfact.where(:id => @current.dailyfact_id).first))
      fdate= to_date(@current.updated_at.day,@current.updated_at.month,@current.updated_at.year)
      tdate= to_date(Date.today.day,Date.today.month,Date.today.year)
      @count = 0
      if fdate != tdate
        if (@next = Dailyfact.where("id > ?", @current.dailyfact_id).order("id ASC").first) && (@next.is_active == true)
          @current.update_attributes(:dailyfact_id => @next.id)
          @currentfact = Dailyfact.where(:id => @next.id).first
        else
          length = Dailyfact.all.length
          num=@current.dailyfact_id
          length.times do |i|
            if @next = Dailyfact.where("id > ?", num).order("id ASC").first
              num=@next.id
              if @next.is_active == true
                @current.update_attributes(:dailyfact_id => @next.id)
                @currentfact = Dailyfact.where(:id => @next.id).first
                break
              else
                @count = @count + 1
              end
            else
              first = Dailyfact.all.first
              num = first.id
            end
          end
        end
      end
      if @currentfact && (@count != Dailyfact.all.length)
        render :json => { :short => "#{@currentfact.shorttext}", :long => "#{@currentfact.longtext}" }, :status => :ok
      else
        render :json => { :message => "Fact is not available"}, :status => :not_found
      end
    else
      render :json => { :message => "Facts is not available or Currentfact is not set" }, :status => :not_found
    end
  end

  private
  def to_date(d,m,y)
    "#{d}-#{m}-#{y}"
  end
end
