class EventsController < ApplicationController
  
  skip_before_filter :load_user_with_api_key, :only => [:show, :all_events, :search]
  before_filter :load_resource, :only => [:rsvp_event, :show, :update, :destroy, :fetch_remaining_review_images, :fetch_specific_info]
  before_filter :ensure_correct_user, :only => [:update, :destroy]
  before_filter :set_response_header, :only => [:show, :fetch_remaining_review_images]
  before_filter :load_user_if_api_key_is_sent, :only => [:search, :show, :rsvp_event]
  before_filter :load_search_results, :only => [:search]
  before_filter :set_response_header_for_search, :only => [:search]
  before_filter :reject_service_type

  def index
    render :json => objects_hash(@user.created_events.includes(:user).order("start_time desc")), :status => :ok
  end

  def all_events
    events = Event.includes(:user).scoped.order("created_at desc").page(params[:page]).per(RESULTS_PER_PAGE)
    render :json => events, :status => :ok
  end

  def fetch_specific_info
    render :json => @event.to_specific_hash(:is_rsvped => @user.has_rsvped_event?(@event), :event_categories => Category.fetch_category_with_product_type('Event')), :status => :ok
  end

  def create
    event = @user.created_events.build(params[:event])
    if event.save
      render :json => { :message => "Event is successfully listed and would be available shortly", :id => event.id }, :status => :ok
    else
      render :json => { :message => event.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
    end
  end

  def rsvp_event
    @rsvp_event = @user.rsvped_user_events.build(:event_id => @event.id, :rsvp => params[:rsvp])
    if @rsvp_event.save
      message, status = "Successfully Rsvp'd event", :ok
    else
      message, status = @rsvp_event.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => { :message => message }, :status => status
  end

  def show
    if @user
      render :json => @event.to_single_hash(:is_rsvped => @user.has_rsvped_event?(@event), :is_favorite => @user.favorite?(@event.id), :type => @event.type), :status => :ok
    else
      render :json => @event.to_single_hash(:is_rsvped => false, :type => @event.type), :status => :ok
    end
  end

  def update
    if @event.update_attributes(params[:event])
      message, status = "Event Updated Successfully", :ok
    else
      message, status = @event.errors.full_messages.join(";\n"), :unprocessable_entity
    end      
    render :json => { :message => message }, :status => status
  end

  def destroy
    if @event.destroy
      message, status = "Event Destroyed Successfully", :ok 
    else
      message, status = @event.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => {:message => message}, :status => status
  end

  def search
    if user_favorite_event = @user.try(:favorite_product_ids)
      render :json => generate_and_add_new_keys(@events.page(params[:page]).per(RESULTS_PER_PAGE), user_favorite_event, :is_favorite), :status => :ok
    else
      render :json => objects_hash(@events.page(params[:page]).per(RESULTS_PER_PAGE)), :status => :ok
    end
  end

  def fetch_remaining_review_images
    render :json => { :all_images => @images.page(params[:page]).per(IMAGES_PER_PAGE).as_json(:only => [:id], :methods => [:thumb_image_url, :iphone_image_url, :display_image_url])}, :status => :ok 
  end

  private


  # def ensure_correct_user_for_event
  #   unless @event.user == @user
  #     render :json => {:message => "You Donot Own this event"}, :status => :unauthorized
  #   end
  # end

  def set_response_header
    super(@event)
  end

  def reject_service_type
    params[:event].try(:delete, :service_type_id)
  end

  def load_user_if_api_key_is_sent
    super([:favorite_products])
  end

  def load_search_results
    @events = Event.includes(:images).where("name like ?", "%#{params[:name]}%")
  end

end
