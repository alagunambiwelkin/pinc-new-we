class FavoritesController < ApplicationController
  before_filter :load_product, :only => [:create, :destroy]
  before_filter :ensure_favorite_is_not_marked, :only => [:create]
  before_filter :load_favorite, :only => [:destroy]

  def index
    render :json => @user.to_favorites_hash, :status => :ok
  end

  def create
    @user.favorite_products << @product
    render :json => { :message => "Favorite Successfully Saved" }, :status => :ok  
  end

  def destroy
    @user.favorite_products.delete(@product)
    # Favorite.where(:user_id => @user.id).where("favorite_product_id = ? and favorite_product_type = ?", @product.id, @product.type).destroy_all
    render :json => { :message => "Favorite Successfully Deleted" }, :status => :ok
  end

  private

  def ensure_favorite_is_not_marked
    # favorite_products.where(:favorite_product_id => product_id).count > 0
    # TODO -> We can use favorite method of user here.
    if @user.favorite_products.where(:favorite_product_id => @product.id).count > 0
      render :json => { :message => "Favorite Already Marked" }, :status => :ok
    end
  end

  def load_favorite
    # NOTE AK - use if and exclude?
    # TODO -> We can use favorite method of user here.
    if @user.favorite_products.where(:favorite_product_id => @product.id).count == 0
      render :json => { :message => "Favorite Not Found" }, :status => :not_found
    end
  end

  def load_product
    unless @product = Product.where(:id => params[:id]).first
      render :json => {"message" => "Product Not Found"}, :status => :not_found
    end
  end
end
