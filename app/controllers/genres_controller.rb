class GenresController < ApplicationController
  skip_before_filter :load_user_with_api_key

  def index
    now_showing = params[:now_showing] == "1"
    if now_showing
      render :json => (Genre.in_theatres({:movies => [:theatres]}, now_showing)) , :status => :ok
    else
      render :json => (Genre.in_theatres(:movies, now_showing)) , :status => :ok
    end
  end
end
