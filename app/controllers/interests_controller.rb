class InterestsController < ApplicationController
  before_filter :load_required_objects

  def index
    # NOTE AK - load all required objects in before_filters
    render :json => generate_and_add_new_keys(@interests, @user_interests, :is_an_interest), :status => :ok
  end

  private

  def load_required_objects
    @interests = Interest.scoped
    @user_interests = @user.interest_ids
  end
end
