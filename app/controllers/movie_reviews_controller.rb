class MovieReviewsController < ApplicationController
  skip_before_filter :load_user_with_api_key,  :only => [:index]
  before_filter :load_user_if_api_key_is_sent, :only => [:index]
  before_filter :load_movie
  before_filter :load_reviews_based_on_user_presence, :only => [:index]

  def index
    render :json => { :reviews => objects_hash(@reviews, @is_review_collection), :average_service_rating => @movie.average_rating, :movie_name => @movie.name }, :status => :ok
  end

  def create
    review = @movie.reviews.build(params[:review]) { |review| review.user = @user }
    if review.save
      render :json => { :message => "Successfully Saved Review", :id => review.id }, :status => :ok
    else 
      render :json => { :message => review.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
    end
  end

  private

    def load_movie
      unless @movie = Movie.where(:id => params[:movie_id]).first
        render :json => {"message" => "Movie Not Found"}, :status => :not_found
      end
    end


    def load_reviews_based_on_user_presence
      if @user 
        @is_review_collection = true
        @reviews = @movie.all_reviews.includes(:user, :images, :votes).order("created_at desc")
      else
        @is_review_collection = false
        @reviews = @movie.all_reviews.includes(:user, :images).order("created_at desc")
      end
    end
end
