class MoviesController < ApplicationController
  skip_before_filter :load_user_with_api_key
  before_filter :load_user_if_api_key_is_sent, :only => [:show]
  before_filter :load_search_results, :only => [:search]
  before_filter :load_resource, :only => [:show]

  def index
    now_showing_field = params[:now_showing] == "1"
    if params[:genre] == "All"
      @movies = Movie.now_showing(now_showing_field)
    else
      @movies = Movie.filter_by_genre(params[:genre]).now_showing(now_showing_field)
    end
    @movies = @movies.having_show if now_showing_field
    render :json => @movies , :status => :ok
  end

  def show
    date = Date.parse(params[:selected_date])
    @theatres = @movie.theatres.where('movie_theatre_shows.movie_date = ?', date).near([params[:latitude], params[:longitude]], 100000).uniq
    if user_favorite_theatres = @user.try(:favorite_product_ids)
      render :json => generate_and_add_new_keys(@theatres, user_favorite_theatres, :is_favorite, {:movie_date => params[:selected_date], :movie_id => @movie.id}), :status => :ok
    else
      render :json => generate_and_add_new_keys(@theatres, [], :is_favorite, {:movie_date => params[:selected_date], :movie_id => @movie.id}), :status => :ok
    end
  end

  def search
    render :json => @movies, :status => :ok
  end

  private

  def load_search_results
    @movies = Movie.where("name like ?", "%#{params[:name]}%")
  end


  def generate_and_add_new_keys(object, object_collection, key, options = {})
    theatres_collection = []
    object.each do |theatre|
      theatres_collection << theatre.to_hash({key => object_collection.include?(theatre.id)}, options)
    end
    theatres_collection.sort_by {|h| [h[:is_favorite] ? 0 : 1, h[:id]]}
  end
end
