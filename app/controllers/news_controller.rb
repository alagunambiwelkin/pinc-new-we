class NewsController < ApplicationController
  skip_before_filter :load_user_with_api_key

  def index
    begin
      xml_data = Net::HTTP.get_response(URI.parse(params[:link])).body
      render :json => Hash.from_xml(xml_data).as_json, :status => :ok
    rescue
      render :json => { "message" => "No data available" }, :status => :not_found
    end
  end
end