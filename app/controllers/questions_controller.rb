class QuestionsController < ApplicationController
  
  skip_before_filter :load_user_with_api_key, :only => [:show, :index, :categories, :filter, :search]
  before_filter :load_resource, :only => [:show, :update, :destroy, :my_answers]
  before_filter :ensure_correct_user, :only => [:update, :destroy]
  before_filter :load_category, :only => [:filter]

  def index
    @questions = Question.includes(:user, :category).recently_asked.unanswered(params[:need_help]).search_with(params[:q])
    render :json => objects_hash(@questions), :status => :ok
	end

  # TODO -> Change name of actions which has my in their name.
  def my_questions
    @questions = @user.questions.recently_asked
    render :json => objects_hash(@questions), :status => :ok
  end

  def my_answered_questions
    @questions = @user.my_answered_questions.recently_asked
    render :json => objects_hash(@questions), :status => :ok
  end

  def categories
    categories = QuestionCategory.all
    render :json => objects_hash(categories), :status => :ok
  end

  def my_answers
    @answers = @question.answers.by_user(@user.id)
    render :json => objects_hash(@answers), :status => :ok
  end

  def create
    @question = @user.questions.build(params[:question])
    if @question.save
      render :json => { :message => "Question has been posted successfully" }
    else
      render :json => { :message => "Question cannot be saved" }
    end
  end

  def search
    @questions = Question.search_with(params[:q]).uniq
    render :json => objects_hash(@questions), :status => :ok
  end

  def update
    if @question.update_attributes(params[:question])
      render :json => { :message => "Question has been updated successfully" }
    else
      render :json => { :message => "Question cannot be updated" }
    end
  end

  def show
    @answers = @question.answers
    render :json => {:answers => objects_hash(@answers.includes(:user).order('updated_at desc')), :question => @question.to_hash}, :status => :ok
  end

  def destroy
    if @question.destroy
      render :json => { :message => "Question is successfully deleted"}
    else
      render :json => { :message => "Question could not be deleted. Please try again"}
    end
  end

  def filter
    @questions = @questions.unanswered(params[:need_help])
    render :json => objects_hash(@questions), :status => :ok
  end

  private

  def load_category
    @category = QuestionCategory.where(:id => params[:category_id]).first
    if @category
      @questions = @category.questions.recently_asked.search_with(params[:q])
    else
      render :json => { :message => "Category not found" }   
    end
  end
end
