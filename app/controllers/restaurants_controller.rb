class RestaurantsController < ApplicationController
	skip_before_filter :load_user_with_api_key#, :only => [:show, :index, :search]
	before_filter :load_restaurant, :only => [:index, :show, :search]
	before_filter :load_user_if_api_key_is_sent, :only => [:search, :show]
	before_filter :load_search_results, :only => [:search]
  	before_filter :set_response_header_for_search, :only => [:search]

	def index
		render :json => @restaurant, :status => :ok
	end

	# def create
	# 	service = Service.new(params[:restaurant])
	#     # service = @user.created_services.build(params[:service])
	#     if service.save
	#       #service.save_time_slots(params[:time_slots])
	#       render :json => { :message => "Restaurant is successfully listed and would be available shortly", :id => service.id }, :status => :ok
	#     else
	#       render :json => { :message => service.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
	#     end
	# end

	def show
		message = @restaurant.where(id:params[:id])
		if message
			render :json => message , :status => :ok
		else
			render :json => { :message => "Restaurant not available" } , :status => :not_found
		end
	end

	def search
	    if user_favorite_services = @user.try(:favorite_product_ids)
	      render :json => generate_and_add_new_keys(@search.page(params[:page]).per(RESULTS_PER_PAGE), user_favorite_services, :is_favorite), :status => :ok
	    else
	      render :json => objects_hash(@search.page(params[:page]).per(RESULTS_PER_PAGE)), :status => :ok
	    end
	end

	private

	def load_restaurant
		type = ServiceType.where(name:"Restaurants").first
		@restaurant = Service.where(service_type_id:type.id)
	end

	def load_user_if_api_key_is_sent
	    super([:favorite_products])
	end

	def load_search_results
		@search = @restaurant.includes([:images, :service_type]).where("name like ?", "%#{params[:name]}%")
	end

end