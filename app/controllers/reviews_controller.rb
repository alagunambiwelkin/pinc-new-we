class ReviewsController < ApplicationController
  skip_before_filter :load_user_with_api_key,  :only => [:index]
  before_filter :load_user_if_api_key_is_sent, :only => [:index]
  before_filter :load_product
  before_filter :ensure_product_is_not_a_commodity
  before_filter :load_reviews_based_on_user_presence, :only => [:index]
  before_filter :load_resource, :only => [:show, :update]

  def index
    render :json => { :reviews => objects_hash(@reviews, @is_review_collection), :average_service_rating => @product.average_rating, :product_name => @product.name }, :status => :ok
  end

  def create
    review = @product.reviews.build(params[:review]) { |review| review.user = @user }
    # review = @product.reviews.build_with_user(params[:review], @user)
    if review.save
      render :json => { :message => "Successfully Saved Review", :id => review.id }, :status => :ok
    else 
      render :json => { :message => review.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
    end
  end

  def show
    render :json => @review.to_custom_review_hash(@user, @product), :status => :ok
  end

  def update
    if @review.update_attributes(params[:review])
      render :json => {}, :status => :ok
    else
      render :json => {:message => @review.errors.full_messages.join(";\n")}, :status => :unprocessable_entity
    end
  end

  private

    def load_product
      unless @product = Product.where(:id => params[:product_id]).first
        render :json => {"message" => "Product Not Found"}, :status => :not_found
      end
    end

    def ensure_product_is_not_a_commodity
      if @product.type == "Commodity"
        render :json => { "message" => "Product Not Eligible For Reviews" }, :status => :not_acceptable
      end
    end

    def load_reviews_based_on_user_presence
      if @user 
        @is_review_collection = true
        @reviews = @product.all_reviews.includes(:user, :images, :votes).order("created_at desc")
      else
        @is_review_collection = false
        @reviews = @product.all_reviews.includes(:user, :images).order("created_at desc")
      end
    end
end
