class ServicesController < ApplicationController
  skip_before_filter :load_user_with_api_key#, :only => [:show, :index, :search]
  before_filter :load_resource, :only => [:show, :fetch_remaining_review_images, :update]
  before_filter :load_user_if_api_key_is_sent, :only => [:search, :show]
  before_filter :ensure_correct_user, :only => [:update, :destroy]
  before_filter :set_response_header, :only => [:show, :fetch_remaining_review_images]
  before_filter :load_search_results, :only => [:search]
  before_filter :set_response_header_for_search, :only => [:search]
  before_filter :load_service_type, :only => [:create]

  def index
    start = ( params[:page].to_i - 1 ) * RESULTS_PER_PAGE
    message = Service.all[ start..(start+RESULTS_PER_PAGE)]
    if message
      render :json => message , :status => :ok
    else
      render :json => { :message => "Service not found" }, :status => :not_found
    end
    #render :json => { :message => Service.all.length } , :status => :ok
  end

  def create
    service = Service.new(params[:service])
    # service = @user.created_services.build(params[:service])
    if service.save
      service.save_time_slots(params[:time_slots])
      render :json => { :message => "Service is successfully listed and would be available shortly", :id => service.id }, :status => :ok
    else
      render :json => { :message => service.errors.full_messages.join(";\n") }, :status => :unprocessable_entity
    end
  end

  def user_services
    render :json => objects_hash(@user.created_services.order("start_time desc")), :status => :ok
  end

  def show
    if @user || @service
      render :json => @service, :status => :ok
    else
      render :json => { :message => "Service not found" }, :status => :ok
    end
    #render :json => @service
  end

  def fetch_remaining_review_images
    render :json => { :all_images => @images.page(params[:page]).per(IMAGES_PER_PAGE).as_json(:only => [:id], :methods => [:thumb_image_url, :iphone_image_url, :display_image_url])}, :status => :ok 
  end

  def update
    if @service.update_attributes(params[:service])
      render :json => {}, :status => :ok
    else
      render :json => {:message => @service.errors.full_messages.join(";\n")}, :status => :unprocessable_entity
    end
  end

  def search
    if user_favorite_services = @user.try(:favorite_product_ids)
      render :json => generate_and_add_new_keys(@services.page(params[:page]).per(RESULTS_PER_PAGE), user_favorite_services, :is_favorite), :status => :ok
    else
      render :json => objects_hash(@services.page(params[:page]).per(RESULTS_PER_PAGE)), :status => :ok
    end
  end

  def destroy
    if @service.destroy
      message, status = "Service Destroyed Successfully", :ok 
    else
      message, status = @event.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => {:message => message}, :status => status
  end

  def service_review
    #@service = Service.where(id:params[:service_id])
    render :json => Review.where(reviewable_id:params[:service_id]), :status => :ok
  end

  def product_review
    ser_review
    pro_review
    if @pro_review.length > 0
      message, status = @pro_review, :ok 
    else
      message, status = { :message => "Reviews not available for this id"}, :unprocessable_entity
    end
    render :json => message, :status => :ok
  end


  private

  def ser_review
    @service_review = Review.where(reviewable_id:params[:service_id])
  end

  def pro_review
    @pro_review = @service_review.where(id:params[:id])
  end

  def load_resource
    super([:reviews])
  end

  def load_user_if_api_key_is_sent
    super([:favorite_products])
  end

  def load_service_type
    @servicetype = ServiceType.where(id:params[:service][:service_type_id])
  end

  def set_response_header
    super(@service)
  end

  def load_search_results
    @services = Service.includes([:images, :service_type]).where("name like ?", "%#{params[:name]}%")
  end
end
