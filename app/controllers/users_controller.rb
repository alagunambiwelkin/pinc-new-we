class UsersController < ApplicationController

  before_filter :load_user, :only => [:login]
  before_filter :load_user_with_api_key, :only => [:logout, :settings, :update, :change_password, :home_menu]
  before_filter :ensure_passwords_are_different, :only => [:change_password]
  before_filter :check_passwords, :only => [:change_password]
  
  def login
    if @user.authenticate(params[:user][:password])
      @user.update_attributes(:device_token => params[:user][:device_token])
      message, status, opts = "Login Successful", :ok, { :api_key => @user.api_key }
    else
      message, status, opts = "Incorrect Email and password combination", :not_acceptable, {}
    end
    render :json => { :message => message }.merge(opts), :status => status
  end

  def create
    user = User.new(params[:user])
    if user.save
      message, status, opts = "Account Successfully Created", :ok, { :api_key => user.api_key }
    else
      message, status, opts = user.errors.full_messages.join(";\n"), :unprocessable_entity, {}
    end
    render :json => { :message => message }.merge(opts), :status => status
  end

  def home_menu
    render :json => @user.to_menu_hash, :status => :ok
  end

  def change_password
    if @user.update_attributes(params[:user])
      message, status = "Successfully Updated Password", :ok
    else
      message, status = @user.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => { :message => message }, :status => status
  end

  def settings
    render :json => @user.to_settings_hash, :status => :ok
  end

  def update
    if @user.update_attributes(params[:user])
      message, status = "Successfully Updated User", :ok
    else
      message, status = @user.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => { :message => message }, :status => status
  end

  private

  def load_user
    unless @user = User.where(:email => params[:user][:email]).first
      render :json => { :message => "Incorrect Email and password combination"}, :status => :not_found
    end
  end

  def ensure_passwords_are_different
    if params[:old_password] == params[:user][:password]
      render :json => { :message => "Old and New Passwords need to be different"}, :status => :not_acceptable
    end
  end

  def check_passwords
    unless @user.authenticate(params[:old_password])
      render :json => { :message => "Incorrect Password Supplied" }, :status => :not_acceptable
    end
  end
end
