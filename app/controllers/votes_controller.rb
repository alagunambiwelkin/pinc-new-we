class VotesController < ApplicationController

  before_filter :load_review
  before_filter :create_vote_type_based_on_style

  def create
    @vote.user, @vote.review   = @user, @review
    if @vote.save
      message, status = "Successfully Saved #{@vote.type.to_s}", :ok
    else
      message, status = @vote.errors.full_messages.join(";\n"), :unprocessable_entity
    end
    render :json => { :message => message }, :status => status
  end

  private
  
  def load_review
    unless @review = Review.where(:id => params[:review_id]).first
      render :json => { "message" => "Review Not Found"}, :status => :not_found    
    end
  end

  def create_vote_type_based_on_style
    @vote = case params[:style]
    when "up"
      Upvote.new
    when "down"
      Downvote.new
    else
      nil
    end
  end
end
