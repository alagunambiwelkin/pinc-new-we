class Contact < ActionMailer::Base
  default :to => DEFAULT_CONTACT_MAIL

  def send_contact_mail(from, subject, body)
    @from  = from
    @body  = body
    mail(:from => @from, :subject => subject)    
  end
end
