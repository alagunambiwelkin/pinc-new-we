class Answer < ActiveRecord::Base
  attr_accessible :content

  validates :content, :user, :question, :presence => true

  belongs_to :question
  belongs_to :user

  scope :by_user, ->(user_id) { where('user_id = ?', user_id) }

  after_create :send_push_notification, :unless => lambda {|answer| answer.user == answer.question.user }

  def answered_by
  	user.name
  end

  def to_hash(new_value = {})
    as_json(:only => [:id, :content], :methods => [:answered_by])
  end

  private

    def send_push_notification
      app = APN::App.last
      device_token = question.user_device_token
      if device_token
        device = create_device(device_token, app.id)
        if create_notification(device)
          app.send_notifications
          device.increment!(:badge_count)
        end
      end
    end

    def create_device(device_token, app_id)
      APN::Device.where(:token => device_token, :app_id => app_id).first_or_create
    end

    def create_notification(device)
      notification = APN::Notification.new
      notification.device = device
      notification.badge = device.badge_count + 1
      notification.sound = true
      notification.alert = "An answer has been posted to your Question."
      notification.custom_properties = {:question_id => question.id }
      notification.save
    end

end
