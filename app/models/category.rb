class Category < ActiveRecord::Base
  #TODO -> product_category_type can be stored in separate table
  attr_accessible :name, :product_category_type

  validates :name, :presence => true
  validates :name, :uniqueness => { :scope => :product_category_type }

  def self.fetch_category_with_product_type(type)
    where(:product_category_type => type).as_json
  end

  def as_json(options = {})
    super(:only => [:id, :name])
  end
end
