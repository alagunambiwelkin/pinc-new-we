class Commodity < Product
  attr_accessible :cost

  validates :currency_id, :cost, :presence => true

  has_one :category
  # validates :cost, :numericality => { :only_integer => true }

  scope :search_with, lambda { |name| includes(:images).where("name like ?", "%#{name}%")}

  def to_hash(new_value = {})
    as_json( :only    => [:id, :name, :address, :contact_number, :type],
             :methods => [:thumb_logo_url, :iphone_image_logo_url]
            ).merge(new_value)
  end

  def to_single_hash(new_value = {})
    as_json( :except  => [ :updated_at, :created_at, 
                           :logo_content_type, :logo_file_name, :logo_file_size, :logo_updated_at, 
                           :service_type_id, :cost_indicator, :reviews_count, :currency_id, :start_time, :end_time], 
             :methods => [:display_image_logo_url, :iphone_image_logo_url],
             :include => [ { :images   => { :only => [:id], :methods => [:thumb_image_url, :iphone_image_url] } },
                           { :currency => { :only => [:locale, :name] } }
                         ]
            ).merge(new_value)
  end
end
