class Currency < ActiveRecord::Base
  attr_accessible :locale, :name

  def to_hash(new_options = {})
    as_json(:except => [:created_at, :updated_at]).merge(new_options)    
  end
end
