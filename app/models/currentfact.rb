class Currentfact < ActiveRecord::Base
  attr_accessible :dailyfact_id, :updated_at

  belongs_to :dailyfact
  
  validates :dailyfact_id, :presence => true, :uniqueness => true
end
