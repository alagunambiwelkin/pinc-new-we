class Dailyfact < ActiveRecord::Base
  attr_accessible :is_active, :longtext, :shorttext, :id

  has_one :currentfact, :dependent => :destroy

  validates :longtext, :presence => true
  validates :shorttext, :presence => true

end
