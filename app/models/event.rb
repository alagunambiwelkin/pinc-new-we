class Event < Product
  include RatingHelper
  attr_accessible :start_time, :end_time, :cost, :website

  belongs_to :user
  has_many :rsvped_user_events

  has_many :reviews, :as => :reviewable, :dependent => :destroy

  has_one :category

  validates :start_time, :end_time, :presence => true
  validate :ensure_end_time_is_greater_than_start_time

  def to_hash(new_value = {})
    as_json(:only     => [:id, :name, :address, :start_time, :end_time, :type],
            :methods  => [:thumb_logo_url, :iphone_image_logo_url],
            :include  => [:user => {:only => [:id, :name]}]
            ).merge(new_value)
  end

  def to_single_hash(new_value = {})
    as_json( :except  => [:updated_at, :created_at, 
                          :logo_content_type, :logo_file_name, :logo_file_size, :logo_updated_at, 
                          :service_type_id, :cost_indicator, :currency_id], 
             :methods => [:display_image_logo_url, :iphone_image_logo_url, :all_images],
             :include => [ { :user      => { :only => [:id, :name]} },
                           { :currency  => { :only => [:id, :locale, :name]} }
                         ]
            ).merge(new_value)
  end

  def to_specific_hash(new_value = {})
    as_json( :except  => [ :updated_at, :created_at, 
                           :logo_content_type, :logo_file_name, :logo_file_size, :logo_updated_at, 
                           :service_type_id, :cost_indicator, :currency_id
                         ],
             :methods => [:display_image_logo_url, :iphone_image_logo_url],
             :include => [ { :user     => { :only => [:id, :name] } },
                           { :currency => { :only => [:id, :locale, :name] } },
                           { :images   => { :only => :id, :methods => [:thumb_image_url, :iphone_image_url] } }
                          ]
            ).merge(new_value)
  end

  def start_time
    super.strftime("%b %d %Y %I:%M %p")
  end

  def end_time
    super.strftime("%b %d %Y %I:%M %p")
  end

  def all_reviews
    Review.where('reviewable_id = ? and reviewable_type = ?', id, type)
  end

  private

  def ensure_end_time_is_greater_than_start_time
    unless DateTime.parse(start_time) < DateTime.parse(end_time)
      errors.add(:start_time, "can't be greater than end time")
    end
  end

end