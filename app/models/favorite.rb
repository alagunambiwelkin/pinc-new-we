class Favorite < ActiveRecord::Base
  attr_accessible :service_id

  belongs_to :user, :counter_cache => true
  belongs_to :favorite_product, :polymorphic => true

  validates :favorite_product_id, :uniqueness => { :scope => :user_id }

  def to_hash(new_value = {})
    as_json( :only    => [:id, :name, :address, :contact_number, :type],
             :methods => [:thumb_logo_url, :iphone_image_logo_url],
             :include => [{ :service_type => {:only => :name} }]
            ).merge(new_value)
  end

  # before_save :set_correct_type

  private
    # TODO -> favorite_product_type is not necessary in favorites table ? It is already stored in products table. -not in use
    # TODO -> Then delete this column from favorites.
    def set_correct_type
      self.favorite_product_type =  self.favorite_product.type
    end
end
