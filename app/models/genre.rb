class Genre < ActiveRecord::Base
  attr_accessible :name

  has_and_belongs_to_many :movies, :uniq => true

  scope :in_theatres, lambda { |joins, now_showing| joins(joins).where('movies.now_showing = ?', now_showing).uniq }
end
