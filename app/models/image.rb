class Image < ActiveRecord::Base
  include PaperclipHelper

  attr_accessible :image
  belongs_to :imageable, :polymorphic => true
  
  has_attachment_file(:image, :styles => DEFAULT_STYLES)
  
  validates_attachment_content_type :image, 
    :content_type => PATTERNS['image'], 
    :message => 'file type is not allowed (only jpeg/png/gif images)', :allow_blank => true

  def image_url
    image.url
  end

  def thumb_image_url
    image.url(:thumb)
  end

  def iphone_image_url
    image.url(:iphone)
  end

  def display_image_url
    image.url(:display)
  end
end
