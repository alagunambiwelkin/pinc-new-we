class Interest < ActiveRecord::Base
  attr_accessible :name

  has_many :users_interests, :dependent => :destroy
  has_many :users, :through => :users_interests

  validates :name, :presence => true, :uniqueness => true

  def to_hash(new_option = {})
    { :id => id,
      :name => name,
    }.merge(new_option)
  end
end
