class Movie < ActiveRecord::Base
  include PaperclipHelper
  include RatingHelper

  attr_accessible :duration, :name, :starring, :synopsis, :now_showing, :picture

  has_many :movie_theatre_shows, :dependent => :destroy
  has_many :theatres, :through => :movie_theatre_shows

  has_and_belongs_to_many :genres, :uniq => true
  has_many :reviews, :as => :reviewable, :dependent => :destroy

  has_attachment_file(:picture, :styles => { :thumb => "87x87!", :small => "44x44!" })

  scope :now_showing, lambda { |now_showing| { :conditions => ["now_showing = ?", now_showing] }}

  scope :having_show, lambda { { :select => 'DISTINCT movies.*', :joins => [:movie_theatre_shows] }}
  
  # TODO -> We can convert this class_method to a scope. - There would be no difference in both the cases so avoiding it.
  def self.filter_by_genre(genre)
    Genre.where(:name => genre).first.movies
  end  

  def as_json(options = {})
    s = {:image_url => image_url(:thumb), :small_image_url => image_url(:small)}
    super(:except => [:created_at, :updated_at, :now_showing, :picture_content_type, :picture_file_name, :picture_file_size, :picture_updated_at], :methods => [:average_rating, :genre_names]).merge(s)
  end

  def image_url(style)
    picture.url(style.to_sym)
  end

  def genre_names
    genres.collect(&:name).join("|")
  end

  def all_reviews
    Review.where('reviewable_id = ? and reviewable_type = ?', id, 'Movie')
  end

end
