class MovieTheatreShow < ActiveRecord::Base
  attr_accessible :movie_date, :movie_id, :start_time, :theatre_id

  belongs_to :movie
  belongs_to :theatre
  validates :movie_date, :movie_id, :start_time, :theatre_id, :presence => true

  def as_json(new_value = {})
    super(:only  => [:start_time]
         ).merge(new_value)
  end

end
