class Product < ActiveRecord::Base
  include PaperclipHelper

  attr_accessible :type # needed for STI
  attr_accessible :name, :address, :description, :contact_number, :latitude, :longitude, :logo, :email # common across all classes
  attr_accessible :images_attributes, :currency_id, :category_id # associations

  validates :name, :address, :presence => true
  validates :contact_number, :email, :presence => true, :unless => lambda { |product| product.type == "Theatre" }
  validates :name, :uniqueness => { :scope => :address, :message => "has been registered with same address. Kindly search" }
  validates :description, :length => { :maximum => 200 }, :allow_blank => true
  validates :contact_number, :length => { :is => 11, :message => "should have %{count} digits" }, :numericality => { :only_integer => true }, :allow_blank => true
  validates :email, :format => {:with => PATTERNS['email']}, :if => lambda { |product| product.errors[:email].blank? }, :allow_blank => true

  has_many :images, :as => :imageable, :dependent => :destroy
  belongs_to :currency
  
  has_attachment_file(:logo, :styles => DEFAULT_STYLES)
  
  accepts_nested_attributes_for :images, :allow_destroy => true

  # NOTE - Meta programming cannot be done here because the singleton class cannot be inherited nor instantiated

  def display_image_logo_url
    logo.url(:display)
  end

  def thumb_logo_url
    logo.url(:thumb)
  end

  def iphone_image_logo_url
    logo.url(:iphone)
  end

  def logo_url
    logo.url
  end


  def save_time_slots(time_slots)
    time_slots.each do |time_slot|
      days = time_slot[:days]
      days.each do |day|
        self.time_slots.create(:start_time => time_slot[:start_time], :end_time => time_slot[:end_time], :day => day, :active => time_slot[:is_active])
      end
    end
  end


  def fetch_time_slots
    time_slots_array = []    
    grouped_time_slots = time_slots.group_by{|e| [e.start_time.strftime("%I:%M %p"), e.end_time.strftime("%I:%M %p")]}
    grouped_time_slots.each_pair do |key, value|
      hash = {}
      hash[:days] = value.collect(&:day)
      hash[:start_time] = key[0]
      hash[:end_time] = key[1]
      time_slots_array << hash
    end
    time_slots_array
  end

  # TODO -> Instead we can make has_many relation to users through favorites - not using it anymore
  # def users
  #   User.includes(:favorites).where('favorites.favorite_product_id = ? and favorites.favorite_product_type = ?', id, type)
  # end

  def is_open?
    return false unless (latitude? && longitude?)
    current_time, day = find_time_zone
    time_slots.available_with_day_and_current_time(day-1, current_time).size > 0
  end

  def find_time_zone
    timezone = Timezone::Zone.new :latlon => [latitude, longitude]
    current_time = timezone.time Time.current
    return current_time.change(:day => 1, :month => 1, :year => 2000), current_time.wday
  end
  
  def all_images
    product_images.as_json(:only => :id, :methods => [:thumb_image_url, :iphone_image_url])
  end

  def product_images(page=1, return_entire_collection = false)
    if return_entire_collection
      return_all_images_of_product_including_review_images
    else
      return_all_images_of_product_including_review_images.page(page).per(IMAGES_PER_PAGE)
    end
  end

  def return_all_images_of_product_including_review_images
    Image.where("(imageable_id = ?  and imageable_type = 'Product') OR (imageable_id in (?) and imageable_type = 'Review')", id, all_reviews.collect(&:id)).order(:created_at)
  end

end
