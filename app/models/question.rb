class Question < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  attr_accessible :content, :details, :category_id, :location

  belongs_to :user
  has_many :answers, :dependent => :destroy
  belongs_to :category, :class_name => "QuestionCategory", :foreign_key => "category_id"

  validates :content, :user, :category_id, :presence => true

  scope :answered, joins(:answers).uniq
  scope :unanswered, ->(need_help) { joins("left join answers on answers.question_id = questions.id").where("answers.question_id is null") if need_help == '1' }

  scope :recently_asked, order('created_at desc')

  scope :search_with, lambda {|keywords| joins("left join answers on answers.question_id = questions.id").where("questions.content like ? or answers.content like ?", "%#{keywords}%", "%#{keywords}%") if keywords.present? }

  def asked_by
    user.name
  end

  def category_name
    category.name
  end

  def posted_time
    "posted #{time_ago_in_words(created_at)} ago"
  end

  def user_device_token
    user.device_token
  end

  def to_hash(new_value = {})
    as_json(:only => [:id, :content, :details, :location], :methods => [:asked_by, :posted_time, :category_name])
  end
end
