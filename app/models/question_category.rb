class QuestionCategory < ActiveRecord::Base
  attr_accessible :name
  has_many :questions, :foreign_key => "category_id"

  def to_hash(new_value = {})
  	as_json(:only => [:id, :name])
  end
end
