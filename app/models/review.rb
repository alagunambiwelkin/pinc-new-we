class Review < ActiveRecord::Base
  
  attr_accessible :content, :rating, :images_attributes, :title
  
  has_many :images, :as => :imageable, :dependent => :destroy

  has_many :votes, :dependent => :destroy
  has_many :upvotes
  has_many :downvotes
  belongs_to :user
  belongs_to :reviewable, :polymorphic => true, :counter_cache => true

  validates :content, :presence => true, :length => { :minimum => 10 }
  validates :rating, :numericality => { :only_integer => true, :less_than_or_equal_to => 5, :greater_than_or_equal_to => 0 }, :allow_nil => true
  
  accepts_nested_attributes_for :images, :allow_destroy => true

  before_save :save_correct_reviewable_type, :unless => lambda { |review| review.reviewable.class == Movie}

  def to_hash(opts = {})
    as_json( :only    => [:id, :content, :rating, :upvotes_count, :downvotes_count, :title],
             :methods => :created_on,
             :include => [ { :user   => { :only => [:name], :methods => [:avatar_thumb_url, :avatar_profile_url, :avatar_iphone_image_url] } },
                           { :images => { :only => [], :methods => [:thumb_image_url, :iphone_image_url] } }
                         ]
           ).merge(opts)
  end

  def to_custom_review_hash(user, product)
    # TODO -> Use scope with_user in vote model and we can use present? here
    { "already_voted" => (votes.where('votes.user_id = ?', user.id).count(:id) > 0), :product_name => product.name }
  end

  def created_on
    created_at.strftime("%d-%b-%Y")
  end

  private

  # TODO saving reviewable_type explicitly is not required
  def save_correct_reviewable_type
    self.reviewable_type = self.reviewable.type
  end
end
