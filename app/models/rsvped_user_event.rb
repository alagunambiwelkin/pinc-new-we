class RsvpedUserEvent < ActiveRecord::Base
  attr_accessible :event_id, :rsvp, :user_id

  belongs_to :event, :class_name => 'Event'
  belongs_to :user

  validates :event_id, :uniqueness => { :scope => :user_id, :message => "has already been rsvped" }
  validates :rsvp, :presence => true
end
