class Service < Product
  include RatingHelper

  attr_accessible :cost_indicator, :website
  attr_accessible :service_type_id

  has_many :time_slots, :foreign_key => :slot_id, :dependent => :destroy, :conditions => { :'time_slots.slot_type' => self.to_s }

  belongs_to :service_type
  has_many :reviews, :as => :reviewable, :dependent => :destroy
  has_one :category

  def to_hash(new_value = {})
    as_json( :only    => [:id, :name, :address, :contact_number, :type],
             :methods => [:thumb_logo_url, :iphone_image_logo_url],
             :include => [{ :service_type => {:only => [:id, :name] } }]
            ).merge(new_value)
  end

  def to_single_hash(new_value = {})
    as_json( :except  => [:updated_at, :created_at, 
                          :logo_content_type, :logo_file_name, :logo_file_size, :logo_updated_at, 
                          :service_type_id, :cost, :currency_id], 
             :methods => [:display_image_logo_url, :iphone_image_logo_url, :all_images],
             :include => [ { :service_type   => { :only => [:id, :name] } },
                           { :currency => { :only => [:locale, :name] } }
                         ]).merge(new_value)
  end

  def all_reviews
    Review.where('reviewable_id = ? and reviewable_type = ?', id, type)
  end

end
