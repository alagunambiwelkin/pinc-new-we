class Theatre < Product
  has_many :movie_theatre_shows
  has_many :movies, :through => :movie_theatre_shows

  geocoded_by :address
  after_validation :geocode


  def to_hash(key = [], options = {})
    json = as_json(:only => [:id, :address], :methods => [:original_name]).merge(:show_times => show_times(options[:movie_id], options[:movie_date])).merge(key)
  end

  def original_name
    name.split("(")[0]
  end

  def show_times(movie_id, movie_date)
    movie_theatre_shows.where(:movie_id => movie_id, :movie_date => Date.parse(movie_date)).as_json
  end

end