class TimeSlot < ActiveRecord::Base
  attr_accessible :active, :day, :end_time, :slot_id, :slot_type, :start_time

  # TODO -> change relationship slot and related variables names
  validates :start_time, :end_time, :day, :presence => true
  belongs_to :slot, :polymorphic => true

  scope :available_with_day_and_current_time, ->(day, current_time) { where('day = ? and end_time > ? and start_time <= ?', day, current_time, current_time) }
end
