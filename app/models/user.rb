class User < ActiveRecord::Base
  include PaperclipHelper
  GENDER = { "MALE" => 0, "FEMALE" => 1 }
  
  attr_accessible :email, :api_key, :gender, :password, :password_confirmation, :date_of_birth, :name, :position, :location, :contact_number, :avatar
  attr_accessible :interest_ids, :device_token

  after_save :has_unique_device_token

  has_secure_password
  
  validates :email, :presence => true, :uniqueness => { :case_sensitive => false, :allow_blank => true }, :format => {:with => PATTERNS['email'], :allow_blank => true}
  validates :password, :length => { :minimum => 5, :allow_blank => true }
  validates :password_confirmation, :presence => true, :if => :password
  validates :gender, :inclusion => { :in => GENDER.values }, :on => :update, :if => :gender
  # validates :contact_number, :format => { :with => PATTERNS['phone'] }, :allow_blank => true
  validates :contact_number, :length => { :is => 11, :message => "should have %{count} digits" }, :numericality => { :only_integer => true }, :allow_blank => true
  
  has_many :favorites, :dependent => :destroy
  has_many :favorite_products, :through => :favorites, :source => :favorite_product, :source_type => "Product"

  has_many :rsvped_user_events, :class_name => :RsvpedUserEvent
  has_many :user_events, :through => :rsvped_user_events, :source => :event, :source_type => 'Product'

  # has_many :favorite_events, :through => :favorites, :source => :favorite_product, :source_type => "Event"
  # has_many :favorite_services, :through => :favorites, :source => :favorite_product, :source_type => "Service"
  # has_many :favorite_commodities, :through => :favorites, :source => :favorite_product, :source_type => "Commodity"

  has_many :created_events, :class_name => "Event"
  has_many :created_services, :class_name => "Service"
  has_many :created_commodities, :class_name => "Commodity"

  has_many :users_interests, :dependent => :destroy
  has_many :interests, :through => :users_interests

  has_many :questions, :dependent => :destroy
  has_many :answers, :dependent => :destroy

  # TODO -> my is not required in the name.
  has_many :my_answered_questions, :through => :answers, :source => :question, :uniq => true


  has_attachment_file :avatar, :styles => USER_STYLES
  
  before_create :set_api_key

  # TODO -> Without class_eval, we can add this method dynamically.
  class_eval do  
    USER_STYLES.keys.each do |style|
      define_method "avatar_#{style}_url" do
        avatar.url(style)
      end
    end
  end

  def avatar_url
    avatar.url
  end

  def to_menu_hash
    as_json(:only => [:name], :methods => [:avatar_profile_url, :avatar_thumb_url])
  end

  def favorite?(product_id)
    # TODO -> We can use scope and present?
    favorites.where(:favorite_product_id => product_id).count > 0
  end

  def self.with_api_key(api_key)
    where(:api_key => api_key).first
  end

  def has_rsvped_event?(event)
    rsvped_user_events.where(:event_id => event.id).present?
  end

  def to_settings_hash
    as_json(:except => [:updated_at, :created_at, :password_digest, :api_key, 
                        :avatar_file_size, :avatar_file_name, :avatar_content_type, 
                        :avatar_updated_at], 
            :methods => [:avatar_profile_url, :avatar_iphone_image_url, :dob])
  end

  def to_favorites_hash
    favorite_products.as_json( :only => [:id, :name, :address, :contact_number, :type],
                               :methods => [:thumb_logo_url, :iphone_image_logo_url]
                              )
  end

  def dob
    date_of_birth.try(:strftime, '%d-%b-%Y')
  end

  private

  def has_unique_device_token
    User.where('device_token = ? and id != ?', self.device_token, self.id).update_all("device_token = null")
  end

  def set_api_key
    begin
      self.api_key = SecureRandom.hex(40)
    end while User.exists?(:api_key => api_key)
  end
end
