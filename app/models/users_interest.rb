class UsersInterest < ActiveRecord::Base
  attr_accessible :interest_id

  belongs_to :user
  belongs_to :interest

  validates :interest_id, :uniqueness => { :scope => :user_id }
end
