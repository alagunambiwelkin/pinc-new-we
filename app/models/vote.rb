class Vote < ActiveRecord::Base
  belongs_to :user
  belongs_to :review, :counter_cache => true

  validates :review_id, :uniqueness => { :scope => :user_id, :message => "has been voted already by you" }
  validates :review_id, :user_id, :presence => true
  
  after_create :increment_counts
  before_destroy :decrement_counts

  protected

  # NOTE AK - create following methods dynamically
  def increment_counts
    Review.increment_counter "#{type.pluralize.underscore}_count", review_id
  end

  def decrement_counts
    Review.decrement_counter "#{type.pluralize.underscore}_count", review_id
  end
end
