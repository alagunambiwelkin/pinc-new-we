require 'rvm/capistrano'
require 'capistrano/ext/multistage'
require 'bundler/capistrano'
require 'delayed/recipes'
require 'new_relic/recipes'
require 'nokogiri'

#load 'deploy/assets'

#set :bundle_flags, ""
set :default_stage, "staging"
#set :ssh_options, { :forward_agent => true }
set :use_sudo, true
#default_run_options[:pty] = true

set :yml_files, %w[database.yml s3.yml newrelic.yml]

after 'deploy:finalize_update', "copy_yml_files", "deploy:cleanup"
after 'deploy:restart', 'unicorn:restart'
after 'deploy', 'deploy:migrate'
after "deploy:stop",    "delayed_job:stop"
after "deploy:start",   "delayed_job:start"
after "deploy:restart", "delayed_job:restart", "newrelic:notice_deployment"

task :copy_yml_files do
  yml_files.each do |file|
    run "cp #{shared_path}/#{file} #{release_path}/config/#{file}"
  end
end

namespace :deploy do
  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end
end

namespace :unicorn do
  
  desc "Zero-downtime restart of Unicorn"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "kill -s USR2 `cat #{shared_path}/tmp/pids/unicorn.pid`"
  end

  desc "Start unicorn"
  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D -E #{rails_env}"
  end

  desc "Stop unicorn"
  task :stop, :roles => :app, :except => { :no_release => true } do
    run "kill -s QUIT `cat #{shared_path}/tmp/pids/unicorn.pid`"
  end  
end

namespace :scraping do 
  desc "starts Scrapping for certain categories"
  task :scrape do
    run("cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake scrap &")
  end

  task :scrape_clear do 
    run("cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake jobs:clear")
  end
end 
