set :application, "pinc-rails"
set :scm, :git
set :repository, "git@bitbucket.org:mdshakeer/pinc2.git"
set :branch, ""

set :user, "vinsol"
set :use_sudo, false

set :domain, ""

set :rails_env, 'production'
set :scm_command, "/usr/bin/git"
set :deploy_to, "/var/www/apps/#{application}"
set :deploy_via, :copy
set :keep_releases, 5

server domain, :app, :web, :db, :primary => true