set :scm, :git
set :application, "pinc-rails-we"
set :repository, "git@bitbucket.org:alagunambiwelkin/pinc-new-we.git"
set :branch, "master"

set :user, "pinc"
set :password, "pincpass"
#set :scm_passphrase, "slimshady"

set :domain, "54.213.75.170"
set :use_sudo, true

set :rails_env, 'staging'
set :scm_command, "/usr/bin/git"
set :deploy_to, "/var/www/apps/pinc-rails/#{application}"
set :deploy_via, :copy
set :keep_releases, 5
set :delayed_job_args, "-n 2"
set :ssh_options, { :forward_agent => true }

server domain, :app, :web, :db, :primary => true
