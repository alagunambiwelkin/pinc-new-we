PATTERNS = {
  'email' => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
  'phone' => /^0(7|8)[0-9]{9}$/,
  'image' => /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/
}

DAYS_NO_HASH      = {"monday" => 0, "tuesday" => 1, "wednesday" => 2, "thursday" => 3, "friday" => 4, "saturday" => 5, "sunday" => 6}
DAYS_FOR_SCRAPE      = {"Mon" => 0, "Tues" => 1, "Wed" => 2, "Thur" => 3, "Fri" => 4, "Sat" => 5, "Sun" => 6}


USER_STYLES       = { :thumb => "20x20!", :profile => "84x84!", :iphone_image => "320" }

DEFAULT_STYLES    = { :thumb => "64x64!", :display => "320x171!", :iphone => "320" }

AWS_BUCKET        = "pinc_app"

AWS_URL           = "https://s3.amazonaws.com/"

PAPERCLIP_OPTIONS = { :path => "/:attachment/:id/:style/:basename.:extension", 
                      :default_url => "#{AWS_URL}#{AWS_BUCKET}/:attachment/:style/missing.png"}

S3_OPTIONS        = { :storage => :s3, :s3_credentials => "config/s3.yml"}

IMAGES_PER_PAGE   = 12

RESULTS_PER_PAGE  = 20

DEFAULT_CONTACT_MAIL = "pincapp@gmail.com"