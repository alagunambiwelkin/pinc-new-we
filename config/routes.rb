Pinc::Application.routes.draw do

  match "/delayed_job" => DelayedJobWeb, :anchor => false

  resources :users, :only => [:create] do 
    post :login, :on => :collection
    # delete :logout, :on => :collection
    put :change_password, :on => :collection
  end
  resources :questions, :except => [:new, :edit] do
    get :categories, :on => :collection
    get :search, :on => :collection
    get :my_answers, :on => :member
    resources :answers, :except => [:new, :edit, :index]
  end

  post :reset_device_badge_count, :controller => "apn_devices", :action => :reset_device_badge_count

  get ":type/categories", :controller => :categories, :action => :categories

  get :home_menu, :controller => :users, :action => :home_menu

  resources :interests, :only => [:index]

  resources :favorites, :only => [:create, :index]

  resources :movies, :only => [:index, :show] do
    get :reviews, :controller => 'movie_reviews', :action => 'index'
    post :reviews, :controller => 'movie_reviews', :action => 'create'
    get :search, :on => :collection
  end

  get 'categories/:category_id/questions', :controller => 'questions', :action => 'filter'  
  get :my_questions, :controller => 'questions', :action => 'my_questions'
  get :my_answered_questions, :controller => 'questions', :action => 'my_answered_questions'

  resources :news, :only => [:index]

  resources :genres, :only => [:index]
  
  resources :services, :only => [:index, :create, :show, :update] do
    get :search, :on => :collection
    get :fetch_remaining_review_images, :on => :member
    # get :categories, :on => :collection
  end

  get 'services/:service_id/reviews', :controller => 'services', :action => 'service_review'
  get '/services/:product_id/reviews/:id', :controller => 'services', :action => 'product_review'

  get :user_services, :controller => :services, :action => :user_services

  resources :commodities, :only => [:index, :create, :show, :update] do
    get :search, :on => :collection
    # get :categories, :on => :collection
  end
  
  resources :currencies, :only => [:index]

  post "reviews/:review_id/vote/:style" => "votes#create"

  resources :events, :except => [:new, :edit] do
    get :search, :on => :collection
    get :fetch_remaining_review_images, :on => :member
    get :fetch_specific_info, :on => :member
    post :rsvp_event, :on => :member
    # get :categories, :on => :collection
  end

  resources :products, :only => [] do 
    resources :reviews, :only => [:index, :create, :show, :update]
  end

  #resources :currentfacts
  
  resources :dailyfacts, :only => [:index]
  
  post '/contact_us' => "contact#contact_us"

  match "/user/settings" => "users#settings"
  put "/user/update" => "users#update"
  delete "/favorite/delete" => "favorites#destroy"
  get "/all_events" => "events#all_events"

  #restaurant
  get "/restaurants", :controller => 'restaurants', :action => 'index'
  get "/restaurants/:id", :controller => 'restaurants', :action => 'show'
end
