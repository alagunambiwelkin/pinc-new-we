# TODO -> Add required indexes in all tables. -done
class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.string :api_key
      t.integer :gender

      t.timestamps
    end
  end
end
