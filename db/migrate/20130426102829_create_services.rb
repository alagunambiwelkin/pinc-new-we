class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :type
      t.string :name
      t.string :address
      t.string :city
      t.string :state
      t.text :description
      
      t.timestamps
    end
  end
end
