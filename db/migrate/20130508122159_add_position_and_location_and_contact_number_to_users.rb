class AddPositionAndLocationAndContactNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :position, :string
    add_column :users, :location, :string
    add_column :users, :contact_number, :string
  end
end
