class AddingIndexToTables < ActiveRecord::Migration
  def up
    add_index(:favorites, :user_id)
    add_index(:favorites, :service_id)
    add_index(:users_interests, :user_id)
    add_index(:users_interests, :interest_id)
  end

  def down
    remove_index(:favorites, :user_id)
    remove_index(:favorites, :service_id)
    remove_index(:users_interests, :user_id)
    remove_index(:users_interests, :interest_id)
  end
end
