class RemoveStateAndCityFromServices < ActiveRecord::Migration
  def up
    remove_column :services, :state
    remove_column :services, :city
  end

  def down
    add_column :services, :city, :string
    add_column :services, :state, :string
  end
end
