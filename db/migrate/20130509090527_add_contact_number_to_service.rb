class AddContactNumberToService < ActiveRecord::Migration
  def change
    add_column :services, :contact_number, :string
  end
end
