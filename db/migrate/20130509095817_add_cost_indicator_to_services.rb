class AddCostIndicatorToServices < ActiveRecord::Migration
  def change
    add_column :services, :cost_indicator, :integer
  end
end
