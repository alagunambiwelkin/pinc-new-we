class AddServiceTypeIdToServices < ActiveRecord::Migration

  def up
    add_column :services, :service_type_id, :integer
    add_index :services, :service_type_id
  end

  def down
    remove_index :services, :service_type_id
    remove_column :services, :service_type_id, :integer
  end
end
