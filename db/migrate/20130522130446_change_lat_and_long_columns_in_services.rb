class ChangeLatAndLongColumnsInServices < ActiveRecord::Migration
  def up
    change_column :services, :latitude , :decimal, :precision => 15, :scale => 10
    change_column :services, :longitude, :decimal, :precision => 15, :scale => 10
  end

  def down
    change_column :services, :latitude, :float
    change_column :services, :longitude, :float
  end
end
