class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.text :content
      t.belongs_to :service
      t.belongs_to :user
      t.timestamps
    end
  end
end
