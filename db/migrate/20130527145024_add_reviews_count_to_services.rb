class AddReviewsCountToServices < ActiveRecord::Migration
  def change
    add_column :services, :reviews_count, :integer
  end
end
