class RemoveReviewsCountFromServices < ActiveRecord::Migration
  def up
    remove_column :services, :reviews_count
  end

  def down
    add_column :services, :reviews_count, :integer
  end
end
