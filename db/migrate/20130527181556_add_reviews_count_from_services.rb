class AddReviewsCountFromServices < ActiveRecord::Migration
  def self.up
    add_column :services, :reviews_count, :integer, :default => 0
  end

  def self.down
    remove_column :services, :reviews_count
  end
end
