class AddUpvotesCountAndDownvotesCountToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :upvotes_count, :integer, :default => 0
    add_column :reviews, :downvotes_count, :integer, :default => 0
  end
end
