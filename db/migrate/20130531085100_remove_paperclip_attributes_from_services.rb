class RemovePaperclipAttributesFromServices < ActiveRecord::Migration
  def up
    remove_column :services, :logo_file_name
    remove_column :services, :logo_content_type
    remove_column :services, :logo_file_size
    remove_column :services, :logo_updated_at
  end

  def down
    add_column :services, :logo_file_name
    add_column :services, :logo_content_type
    add_column :services, :logo_file_size
    add_column :services, :logo_updated_at
  end
end
