class AddServiceAttributesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :cost_indicator, :integer
    add_column :products, :service_type_id, :integer
    add_column :products, :reviews_count, :integer, :default => 0
  end
end
