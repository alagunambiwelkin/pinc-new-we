class AddCurrencyIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :currency_id, :integer
  end
end
