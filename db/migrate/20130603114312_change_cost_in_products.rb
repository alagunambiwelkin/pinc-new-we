class ChangeCostInProducts < ActiveRecord::Migration
  def up
    change_column :products, :cost, :decimal, :precision => 15, :scale => 2, :default => 0.00
  end

  def down
    change_column :products, :cost, :integer
  end
end
