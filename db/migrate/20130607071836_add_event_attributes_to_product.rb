class AddEventAttributesToProduct < ActiveRecord::Migration
  def change
    add_column :products, :start_time, :datetime
    add_column :products, :end_time, :datetime
    add_column :products, :user_id, :integer
  end
end
