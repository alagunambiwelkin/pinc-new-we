class RemoveServiceIdAndEventIdFromFavorites < ActiveRecord::Migration
  def up
    remove_column :favorites, :service_id
    remove_column :favorites, :event_id
  end

  def down
    add_column :favorites, :event_id, :integer
    add_column :favorites, :service_id, :integer
  end
end
