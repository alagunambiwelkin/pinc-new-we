class AddFavoriteProductIdAndFavoriteProductTypeToFavorites < ActiveRecord::Migration
  def change
    add_column :favorites, :favorite_product_id, :integer
    add_column :favorites, :favorite_product_type, :string
  end
end
