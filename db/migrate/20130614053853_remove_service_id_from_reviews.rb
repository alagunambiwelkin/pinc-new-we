class RemoveServiceIdFromReviews < ActiveRecord::Migration
  def up
    remove_column :reviews, :service_id
  end

  def down
    add_column :reviews, :service_id, :integer
  end
end
