class CreateRsvpedUserEvents < ActiveRecord::Migration
  def change
    create_table :rsvped_user_events do |t|
      t.integer :user_id
      t.integer :event_id
      t.integer :rsvp

      t.timestamps
    end
  end
end
