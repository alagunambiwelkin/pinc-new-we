class AddAttributesToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :name, :string
    add_column :categories, :product_category_type, :string
  end
end
