class CreateTimeSlots < ActiveRecord::Migration
  def change
    create_table :time_slots do |t|
      t.integer :day
      t.datetime :start_time
      t.datetime :end_time
      t.integer :slot_id
      t.string :slot_type
      t.boolean :active

      t.timestamps
    end
  end
end
