class CreateMovieTheatreShows < ActiveRecord::Migration
  def change
    create_table :movie_theatre_shows do |t|
      t.integer :movie_id
      t.integer :theatre_id
      t.string :start_time
      t.datetime :movie_date

      t.timestamps
    end
  end
end
