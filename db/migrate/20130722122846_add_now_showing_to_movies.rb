class AddNowShowingToMovies < ActiveRecord::Migration
  def change
    add_column :movies, :now_showing, :boolean, :default => true
  end
end
