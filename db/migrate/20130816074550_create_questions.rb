class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :content
      t.text :details
      t.string :category
      t.integer :user_id

      t.timestamps
    end
  end
end
