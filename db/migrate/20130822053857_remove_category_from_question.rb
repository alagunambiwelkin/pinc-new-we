class RemoveCategoryFromQuestion < ActiveRecord::Migration
	def change
    add_column :questions, :category_id, :integer
    remove_column :questions, :category
	end
end
