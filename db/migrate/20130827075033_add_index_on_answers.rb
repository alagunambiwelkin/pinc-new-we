class AddIndexOnAnswers < ActiveRecord::Migration
  def change
    add_index(:answers, [:question_id])
    add_index(:answers, [:user_id])
    add_index(:answers, [:user_id, :question_id])
  end
end
