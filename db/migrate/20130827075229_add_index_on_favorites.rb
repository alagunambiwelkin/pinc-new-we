class AddIndexOnFavorites < ActiveRecord::Migration
  def change
    add_index(:favorites, [:favorite_product_id, :favorite_product_type])
  end
end
