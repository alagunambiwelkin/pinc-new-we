class AddIndexOnGenre < ActiveRecord::Migration
  def change
    add_index(:genres_movies, [:movie_id])
    add_index(:genres_movies, [:genre_id])
  end
end
