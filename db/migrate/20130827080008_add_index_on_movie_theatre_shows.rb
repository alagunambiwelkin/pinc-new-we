class AddIndexOnMovieTheatreShows < ActiveRecord::Migration
  def change
    add_index(:movie_theatre_shows, [:movie_id])
  end
end
