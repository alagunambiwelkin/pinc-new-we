class AddIndexOnProducts < ActiveRecord::Migration
  def change
    add_index(:products, [:currency_id])
    add_index(:products, [:user_id])
    add_index(:products, [:category_id])
  end
end
