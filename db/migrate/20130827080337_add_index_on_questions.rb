class AddIndexOnQuestions < ActiveRecord::Migration
  def change
    add_index(:questions, [:category_id])
    add_index(:questions, [:user_id])
  end
end
