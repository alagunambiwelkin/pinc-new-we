class AddIndexOnRsvpedUserEvents < ActiveRecord::Migration
  def change
    add_index(:rsvped_user_events, [:user_id])
  end
end
