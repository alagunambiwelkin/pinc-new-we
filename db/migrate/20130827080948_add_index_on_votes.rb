class AddIndexOnVotes < ActiveRecord::Migration
  def change
    add_index(:votes, [:review_id])
    add_index(:votes, [:user_id])
  end
end
