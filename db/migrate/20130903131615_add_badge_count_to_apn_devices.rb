class AddBadgeCountToApnDevices < ActiveRecord::Migration
  def change
    add_column :apn_devices, :badge_count, :integer, :default => 0
  end
end
