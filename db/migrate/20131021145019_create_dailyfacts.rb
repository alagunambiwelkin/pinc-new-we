class CreateDailyfacts < ActiveRecord::Migration
  def change
    create_table :dailyfacts do |t|
      t.text :shorttext
      t.text :longtext
      t.boolean :is_active, :default => false

      t.timestamps
    end
  end
end
