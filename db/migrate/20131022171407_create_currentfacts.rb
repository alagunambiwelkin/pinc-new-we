class CreateCurrentfacts < ActiveRecord::Migration
  def change
    create_table :currentfacts do |t|
      t.integer :dailyfact_id

      t.timestamps
    end
  end
end
