# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131022171407) do

  create_table "answers", :force => true do |t|
    t.text     "content"
    t.integer  "question_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"
  add_index "answers", ["user_id", "question_id"], :name => "index_answers_on_user_id_and_question_id"
  add_index "answers", ["user_id"], :name => "index_answers_on_user_id"

  create_table "apn_apps", :force => true do |t|
    t.text     "apn_dev_cert"
    t.text     "apn_prod_cert"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "apn_device_groupings", :force => true do |t|
    t.integer "group_id"
    t.integer "device_id"
  end

  add_index "apn_device_groupings", ["device_id"], :name => "index_apn_device_groupings_on_device_id"
  add_index "apn_device_groupings", ["group_id", "device_id"], :name => "index_apn_device_groupings_on_group_id_and_device_id"
  add_index "apn_device_groupings", ["group_id"], :name => "index_apn_device_groupings_on_group_id"

  create_table "apn_devices", :force => true do |t|
    t.string   "token",              :default => "", :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.datetime "last_registered_at"
    t.integer  "app_id"
    t.integer  "badge_count",        :default => 0
  end

  add_index "apn_devices", ["token"], :name => "index_apn_devices_on_token"

  create_table "apn_group_notifications", :force => true do |t|
    t.integer  "group_id",          :null => false
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.text     "custom_properties"
    t.datetime "sent_at"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "apn_group_notifications", ["group_id"], :name => "index_apn_group_notifications_on_group_id"

  create_table "apn_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "app_id"
  end

  create_table "apn_notifications", :force => true do |t|
    t.integer  "device_id",                        :null => false
    t.integer  "errors_nb",         :default => 0
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.datetime "sent_at"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.text     "custom_properties"
  end

  add_index "apn_notifications", ["device_id"], :name => "index_apn_notifications_on_device_id"

  create_table "apn_pull_notifications", :force => true do |t|
    t.integer  "app_id"
    t.string   "title"
    t.string   "content"
    t.string   "link"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "launch_notification"
  end

  create_table "categories", :force => true do |t|
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "name"
    t.string   "product_category_type"
  end

  create_table "currencies", :force => true do |t|
    t.string   "name"
    t.string   "locale"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "currentfacts", :force => true do |t|
    t.integer  "dailyfact_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "dailyfacts", :force => true do |t|
    t.text     "shorttext"
    t.text     "longtext"
    t.boolean  "is_active",  :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "favorites", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "favorite_product_id"
    t.string   "favorite_product_type"
  end

  add_index "favorites", ["favorite_product_id", "favorite_product_type"], :name => "index_favorites_on_favorite_product_id_and_favorite_product_type"
  add_index "favorites", ["user_id"], :name => "index_favorites_on_user_id"

  create_table "genres", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "genres_movies", :force => true do |t|
    t.integer "genre_id"
    t.integer "movie_id"
  end

  add_index "genres_movies", ["genre_id"], :name => "index_genres_movies_on_genre_id"
  add_index "genres_movies", ["movie_id"], :name => "index_genres_movies_on_movie_id"

  create_table "images", :force => true do |t|
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "images", ["imageable_id", "imageable_type"], :name => "index_images_on_imageable_id_and_imageable_type"

  create_table "interests", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "movie_theatre_shows", :force => true do |t|
    t.integer  "movie_id"
    t.integer  "theatre_id"
    t.string   "start_time"
    t.datetime "movie_date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "movie_theatre_shows", ["movie_id"], :name => "index_movie_theatre_shows_on_movie_id"

  create_table "movies", :force => true do |t|
    t.string   "name"
    t.string   "duration"
    t.string   "starring"
    t.text     "synopsis"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "now_showing",          :default => true
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "reviews_count",        :default => 0
  end

  create_table "products", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "contact_number"
    t.decimal  "latitude",          :precision => 15, :scale => 10
    t.decimal  "longitude",         :precision => 15, :scale => 10
    t.string   "email"
    t.text     "description"
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "type"
    t.integer  "cost_indicator"
    t.integer  "service_type_id"
    t.integer  "reviews_count",                                     :default => 0
    t.integer  "currency_id"
    t.decimal  "cost",              :precision => 15, :scale => 2,  :default => 0.0
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "user_id"
    t.string   "website"
    t.integer  "category_id"
  end

  add_index "products", ["category_id"], :name => "index_products_on_category_id"
  add_index "products", ["currency_id"], :name => "index_products_on_currency_id"
  add_index "products", ["user_id"], :name => "index_products_on_user_id"

  create_table "question_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "questions", :force => true do |t|
    t.text     "content"
    t.text     "details"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "category_id"
    t.string   "location"
  end

  add_index "questions", ["category_id"], :name => "index_questions_on_category_id"
  add_index "questions", ["user_id"], :name => "index_questions_on_user_id"

  create_table "reviews", :force => true do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.integer  "rating"
    t.integer  "upvotes_count",   :default => 0
    t.integer  "downvotes_count", :default => 0
    t.integer  "votes_count",     :default => 0
    t.integer  "reviewable_id"
    t.string   "reviewable_type"
    t.string   "title"
  end

  add_index "reviews", ["reviewable_id", "reviewable_type"], :name => "index_reviews_on_reviewable_id_and_reviewable_type"
  add_index "reviews", ["user_id"], :name => "index_reviews_on_user_id"

  create_table "rsvped_user_events", :force => true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.integer  "rsvp"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "rsvped_user_events", ["user_id"], :name => "index_rsvped_user_events_on_user_id"

  create_table "service_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "services", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.text     "description"
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
    t.string   "contact_number"
    t.integer  "cost_indicator"
    t.decimal  "latitude",        :precision => 15, :scale => 10
    t.decimal  "longitude",       :precision => 15, :scale => 10
    t.integer  "service_type_id"
    t.integer  "reviews_count",                                   :default => 0
    t.string   "email"
  end

  add_index "services", ["service_type_id"], :name => "index_services_on_service_type_id"

  create_table "time_slots", :force => true do |t|
    t.integer  "day"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "slot_id"
    t.string   "slot_type"
    t.boolean  "active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "api_key"
    t.integer  "gender"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.date     "date_of_birth"
    t.string   "name"
    t.string   "position"
    t.string   "location"
    t.string   "contact_number"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "favorites_count",     :default => 0
    t.string   "device_token"
  end

  add_index "users", ["api_key"], :name => "index_users_on_api_key"

  create_table "users_interests", :force => true do |t|
    t.integer  "user_id"
    t.integer  "interest_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "users_interests", ["interest_id"], :name => "index_users_interests_on_interest_id"
  add_index "users_interests", ["user_id"], :name => "index_users_interests_on_user_id"

  create_table "votes", :force => true do |t|
    t.string   "type"
    t.integer  "user_id"
    t.integer  "review_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "votes", ["review_id"], :name => "index_votes_on_review_id"
  add_index "votes", ["user_id"], :name => "index_votes_on_user_id"

end
