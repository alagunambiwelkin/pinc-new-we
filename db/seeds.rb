["Hotel", "Restaurant", "Grocery", "Pharmacy", "Hair Saloon", "Services", "Medical", "Other"].each do |service|
  ServiceType.create(:name => service)
end 

["Social Events", "News", "Traffic Updates", "Restaurants", "Music & Concerts", "Weather", "Outdoors"].each do |interest|
  Interest.create(:name => interest)
end
