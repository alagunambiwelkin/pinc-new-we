module PaperclipHelper
  extend ActiveSupport::Concern

  module ClassMethods
    def has_attachment_file(column_name, options = {})
      if Rails.env.production? || Rails.env.staging? || Rails.env.development?
        has_attached_file column_name, options.merge(S3_OPTIONS).merge(PAPERCLIP_OPTIONS)
      else
        has_attached_file column_name, options
      end
      process_in_background column_name
    end
  end
end