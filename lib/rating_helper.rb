module RatingHelper

  def average_rating
    #TODO -> Do not fetch records.Perform only sum.
    ratings_sum = all_reviews.sum { |review| review.rating.to_i }
    round_to_nearest_half(reviews_count, ratings_sum)
  end

  private
    def round_to_nearest_half(rating_count, ratings_sum)
      return 0.0 if rating_count == 0
      average_rating = (ratings_sum.to_f / all_reviews.count)
      unless (ratings_sum % reviews_count == 0)
        average_rating.floor + (( (average_rating - average_rating.floor) > 0.5) ? 1 : 0.5)
      else
        average_rating
      end
    end
end