# encoding: utf-8
require 'nokogiri'
require 'open-uri'

module Scrapper
  module Afritickets
    extend ActiveSupport::Concern
    module ClassMethods
      BASE_URL                       = "http://www.afritickets.com/site/"
      LANDING_URL                    = "http://www.afritickets.com/site/events.asp"

      def fetch_events_from_category
        doc = Nokogiri::HTML(open(LANDING_URL))
        categories_list = doc.css('div.club_list ul li').slice(1..-2)
        categories_list.each do |category_block|
          category = category_block.at_css('a')
          name = category.text.squish
          @category = Category.where(:name => name, :product_category_type => "Event").first_or_create
          puts @category.name
          link = category.attr('href')
          fetch_events(link)
        end
      end


      def fetch_events(link)
        doc = Nokogiri::HTML(open(BASE_URL+link))
        event_block = doc.css('div.center_block')
        event_block.each do |event|
          detail_link = event.at_css('a.comp')
          detail_url = detail_link.attr('href')
          doc_for_event_detail = Nokogiri::HTML(open(BASE_URL+detail_url))
          set_event_details(doc_for_event_detail)
        end
      end


      def set_event_details(doc)
        event_name = doc.at_css('h1').text.squish
      end

    end
  end
end
