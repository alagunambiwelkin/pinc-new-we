# encoding: utf-8
require 'nokogiri'
require 'open-uri'

module Scrapper
  module Genesis
    extend ActiveSupport::Concern
    module ClassMethods
      BASE_URL                       = "http://www.genesisdeluxecinemas.com/"
      LANDING_URL                    = "http://www.genesisdeluxecinemas.com/NowShowing.html"
      COMING_SOON_URL                = "http://www.genesisdeluxecinemas.com/FilmCominsoon.html"


      def fetch_all_movies
        doc = Nokogiri::HTML(open(LANDING_URL))
        theatre_name = doc.at_css("title").text
        options = doc.css("form select.form option")
        start_date = doc.at_css('form').children.first.text.squish.split("-")[0].split(" ").drop(1).join(" ")
        start_date = DateTime.strptime(start_date, "%d %b")
        options.drop(1).each do |option|
          path = option['value']
          full_name = theatre_name + "("+option.text + ")"
          @theatre = Theatre.where(:name => full_name).first
          puts @theatre.name
          doc_for_theatre = Nokogiri::HTML(open(BASE_URL+path))
          movie_listings = doc_for_theatre.css("div#content div.news")
          populate_movies_for_theatre(@theatre, movie_listings, start_date)
        end
        puts "Coming Soon"
        fetch_coming_soon_movies
      end

      def fetch_coming_soon_movies
        doc = Nokogiri::HTML(open(COMING_SOON_URL))
        movie_listing = doc.css('div.news_item').drop(1)
        movie_listing.each do |movie_block|
          name = movie_block.at_css('a.title').text.squish
          @movie = Movie.where(:name => name).first_or_initialize
          puts @movie.name
          if @movie.new_record?
            set_movie_details(movie_block, false)
            set_genres_for_movie(movie_block)
          end
        end
      end


      def populate_movies_for_theatre(theatre, movie_listings, start_date)
        movie_listings.each do |movie_block|
          movie = movie_block.at_css('div.news_item')
          name = movie.at_css('a.title').text.squish
          @movie = Movie.where(:name => name).first_or_initialize
          puts @movie.name
          if @movie.new_record?
            set_movie_details(movie)
            set_genres_for_movie(movie)
          end
          set_movie_time_slots(movie, start_date, theatre)
        end
      end


      def set_movie_details(movie, now_showing = true)
        @movie.starring = movie.at("span:contains('Starring')").next.text.squish
        @movie.duration = movie.at("span:contains('Running Time')").next.text.squish if movie.at("span:contains('Running Time')")
        @movie.synopsis = movie.at_css('div.details_text').children.first.text.squish
        logo_url = movie.at_css('img.wrap_me').attr('src').squish
        @movie.picture = URI.parse(BASE_URL+logo_url)
        @movie.now_showing = now_showing
        @movie.save
      end

      def set_genres_for_movie(movie)
        genres = movie.at("span:contains('Genre')").next.text.squish.split(" | ")
        genres.each do |genre|
          @genre = Genre.where(:name => genre).first_or_create
          @movie.genres << @genre
        end
      end

      def date_of_next(day_range, start_date)
        date_range =[]
        day_range.each do |day|

          date  = Date.parse(day.strip)
          date_diff = (date - start_date).to_i
          delta = date >= start_date ? (date_diff >= 7 ? (date_diff - 7) : date_diff) : 7

          date_range << (date >= start_date ? start_date + delta : date + delta)
        end
        date_range
      end

      def return_valid_avatar(logo_url)
        begin
          (URI.parse(BASE_URL+url)) if logo_url
        rescue Exception => e
          Delayed::Worker.logger.debug e.message
          nil
        end
      end

      def find_start_date(day, start_date)
        date  = Date.parse(day.strip)
        date_diff = (date - start_date).to_i
        delta = date >= start_date ? (date_diff >= 7 ? (date_diff - 7) : date_diff) : 7
        puts date
        date >= start_date ? start_date + delta : date + delta
      end



      def set_movie_time_slots(movie, start_date, theatre)
        show_timings_for_all_days = movie.xpath('.//*[preceding-sibling::a[contains(@class, "title")] and following-sibling::span[contains(@class, "by") and position() = 2]]').map { |n| n.next.text.try(:squish) }.reject {|s| s == "Genre:" || s.blank? }
        show_timings_for_all_days.each do |show_timings|
          show_timings_array = show_timings.split(":")
          puts show_timings_array[0]
          show_timings_array[0] = show_timings_array[0].gsub("-", "") if show_timings_array[0].include?("& -")
          if show_timings_array[0].include?("-")
            day_range = show_timings_array[0].split("-")
            no_of_days = DAYS_FOR_SCRAPE[day_range[0].strip] - DAYS_FOR_SCRAPE[day_range[1].strip]
            range_start_date = find_start_date(day_range[0], start_date)
            end_date = range_start_date + no_of_days
            date_range = (range_start_date..end_date).to_a
          elsif show_timings_array[0].include?("&")
            day_range = show_timings_array[0].split("&")
            date_range = date_of_next(day_range, start_date)
          elsif show_timings_array[0].include?("(")
            day_range = [show_timings_array[0].split("(")[0]]
            date_range = date_of_next(day_range, start_date)
          else
            day_range = [show_timings_array[0]]
            date_range = date_of_next(day_range, start_date)
          end
            
          timings = show_timings_array[1].split(",")
          date_range.each do |date|
            timings.each do |time|
              puts "#{time} movie time"
              puts "#{date} movie date"
              @movie_show = @movie.movie_theatre_shows.create(:theatre_id => theatre.id, :movie_date => date.beginning_of_day, :start_time => time)
            end
          end
        end
      end

    end
  end
end
