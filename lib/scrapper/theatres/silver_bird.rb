# encoding: utf-8
require 'nokogiri'
require 'open-uri'

module Scrapper
  module SilverBird
    extend ActiveSupport::Concern
    module ClassMethods
      BASE_URL                       = "http://silverbirdcinemas.com/"
      LANDING_URL                    = "http://silverbirdcinemas.com/lagos"

      # def fetch_theatre_details
      #   doc = Nokogiri::HTML(open(SEED_URL))
      #   name = doc.at_css("h2").text
      #   address = doc.at_css("div.cin_address p.gk_house:nth-child(2)").text
      #   Theatre.create(:name => name, :address => address)
      # end


      def fetch_all_movies
        doc = Nokogiri::HTML(open(LANDING_URL))
        theatre_name = doc.at_css("div#top-nav span").text
        links = doc.css('div#k2ModuleBox131 ul.level0 li a')
        date_tag = doc.at_css('strong')
        start_date = date_tag.text.squish.gsub(/[a-z]/, "") + date_tag.next.text.squish.split("-")[0].squish
        puts start_date
        start_date = DateTime.strptime(start_date, "%d %b")
        links.each_with_index do |link, index|
          path = link.attr('href')
          doc_for_theatre = Nokogiri::HTML(open(BASE_URL+path))
          if(index != 5)
            text = doc_for_theatre.at_css('div.itemListCategory h2').text
            full_name = theatre_name + "("+text + ")"
            puts full_name
            @theatre = Theatre.where(:name => full_name).first
            puts @theatre.name
            next unless @theatre
            movie_listings = doc_for_theatre.css('div.itemContainer')
            populate_movies_for_theatre(@theatre, movie_listings, start_date)
          else            
            fetch_coming_soon_movies(doc_for_theatre)
          end
        end
      end


      def fetch_coming_soon_movies(doc)
        movie_listing = doc.css('div.catItemView.groupLeading')
        movie_listing.each do |movie_block|
          name = movie_block.at_css('h3.catItemTitle a').text.squish
          @movie = Movie.where(:name => name).first_or_initialize
          puts @movie.name
          if @movie.new_record?
            set_movie_details_for_coming_soon(movie_block)
          end
        end
      end

      def set_movie_details_for_coming_soon(movie)
        @movie.starring = movie.at_css('p:nth-child(5)').text.squish if movie.at_css('p:nth-child(5)')
        @movie.synopsis = movie.at_css('div.catItemIntroText p').text.squish if movie.at_css('div.catItemIntroText p')
        avatar_url = movie.at_css('.catItemImage img').attr('src').squish if movie.at_css('.catItemImage img')
        @movie.picture = return_valid_avatar(avatar_url)
        @movie.now_showing = false
        @movie.save
      end



      def populate_movies_for_theatre(theatre, movie_listings, start_date)
        movie_listings.each do |movie|
          # movie = movie_block.at_css('div.catItemView')
          name = movie.at_css('h3.catItemTitle a').text.squish.split("(")[0].strip
          @movie = Movie.where(:name => name).first_or_initialize
          puts @movie.name
          if @movie.new_record?
            set_movie_details(movie)
            set_genres_for_movie(movie)
          end
          set_movie_time_slots(movie, start_date, theatre) if @movie.valid?
        end
      end


      def set_movie_details(movie)
        @movie.starring = movie.at_css('.typeTextarea .catItemExtraFieldsValue').text.squish if movie.at_css('.typeTextarea .catItemExtraFieldsValue')
        @movie.duration = movie.at_css('ul ul ul ul ul ul ul .odd.typeTextfield .catItemExtraFieldsValue').text.squish if movie.at_css('ul ul ul ul ul ul ul .odd.typeTextfield .catItemExtraFieldsValue')
        @movie.synopsis = movie.at_css('div.catItemIntroText p').text.squish if movie.at_css('div.catItemIntroText p')
        avatar_url = movie.at_css('.catItemImage img').attr('src').squish if movie.at_css('.catItemImage img')
        @movie.picture = return_valid_avatar(avatar_url)
        @movie.save
      end

      def set_genres_for_movie(movie)
        genres = movie.at_css('.typeSelect .catItemExtraFieldsValue').text.squish.split("/")
        genres.each do |genre|
          @genre = Genre.where(:name => genre).first_or_create
          @movie.genres << @genre
        end
      end

      def date_of_next(day, start_date)
        # date_range =[]
        # day_range.each do |day|
          date  = Date.parse(day.strip)
          date_diff = (date - start_date).to_i
          delta = date >= start_date ? (date_diff >= 7 ? (date_diff - 7) : date_diff) : 7

          date >= start_date ? start_date + delta : date + delta
          # date + delta
        # end
        # date_range
      end


      def return_valid_avatar(logo_url)
        begin
          (URI.parse(BASE_URL+logo_url)) if logo_url
        rescue OpenURI::HTTPError => e
          Delayed::Worker.logger.debug e.message
          nil
        end
      end

      def set_movie_time_slots(movie, start_date, theatre)
        movie_shows_hash = {}
        details = movie.at_css('.catItemExtraFields')
        details.css('ul').each do |detail|
          detail.at_css('li span.catItemExtraFieldsLabel').text.squish.gsub("Viewing Time - ") do |match|
            movie_shows = detail.at_css('li span.catItemExtraFieldsValue').text.squish.split(",")
            day = detail.at_css('li span.catItemExtraFieldsLabel').text.squish.gsub("Viewing Time - ","")
            movie_shows_hash[day] = movie_shows
          end
        end

        movie_shows_hash.each do |day, times|
          puts day+" => "+times.join(",")
          times.each do |time|
            @movie_show = @movie.movie_theatre_shows.create(:theatre_id => theatre.id, :movie_date => date_of_next(day, start_date), :start_time => time)
          end
        end
      end

    end
  end
end
