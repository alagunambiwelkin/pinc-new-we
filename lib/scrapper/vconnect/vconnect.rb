# encoding: utf-8
require 'nokogiri'
require 'open-uri'

module Scrapper
  module VConnect
    extend ActiveSupport::Concern
    module ClassMethods
      SEED_URL                       = "http://www.vconnect.com/nigeria/categories-from-a"
      BASE_URL                       = "http://www.vconnect.com"
      CSS_FOR_CATEGORY_INDEX_PAGE    = ".hot-browselist"
      SPECIFIC_CATEGORIES            = [ "Accommodation", "Restaurants", "Hotels", "Groceries", "Pharmacy"]
      SPECIAL_CATEGORIES             = { 
                                        "Medical" => [ "Doctors",
                                                       "Doctors Child Specialist",
                                                       "Doctors Dentists",
                                                       "Doctors ENT Specialist (Ear, Nose & Throat Specialist)",
                                                       "Doctors Gynaecology",
                                                       "Doctors Nephrologists",
                                                       "Doctors Ophthalmologist",
                                                       "Doctors Orthopedist",
                                                       "Doctors Physiotherapy",
                                                       "Doctors Radiologist",
                                                       "Doctors Skin Specialist",
                                                       "Dieticians",
                                                       "Healthcare",
                                                       "Hospitals",
                                                       "Medical Equipment Dealers",
                                                       "Medical Equipment Manufacturers",
                                                       "Medical Laboratory",
                                                       "Medical Gases",
                                                       "Homeopathic Medicine"
                                                     ],
                                        "Beauty & Wellness" => [ "Beauty Parlours", 
                                                                 "Beauty Products",
                                                                 "Barbers",
                                                                 "Beauty Schools",
                                                                 "Massage Parlours",
                                                                 "Health & Beauty",
                                                                 "Hair Care",
                                                                 "Hair Cream",
                                                                 "Skin Care",
                                                                 "Skin Care Consultancy"
                                                               ]
                                      }
      CSS_FOR_CAT_PRODUCT_INDEX_PAGE = ".browsecat_wrapll, .browsecat_wraplr"
      PAGINATION_CSS_PATTERN         = "#ctl00_cphMiddleContainer_PagesDisplayTop a"
      PAGINATION_FOR_PRODUCTS_PATTERN= "#ctl00_cphMiddleContainer_anchorPageNextTop"
      PAGINATION_LAST_PAGE_PATTERN   = "#ctl00_cphMiddleContainer_anchorPageLastTop"
      LAST_LINK_PATTERN              = /page(\d{1,})/

      def fetch_all
        puts ":::::::::STARTING TO FETCH BASE URL::::::::::::"
        starting_doc       = Nokogiri::HTML(open(SEED_URL))
        categories_links   = starting_doc.css(CSS_FOR_CATEGORY_INDEX_PAGE).search("a").collect{ |a| "#{BASE_URL}#{a['href'] }" }
        fetch_category_business_listing_pages(categories_links)
      end

      def fetch_category_business_listing_pages(categories_links)
        categories_links.each do |categories_link|
          ScrapingJob.delay.fetch_category_products_page(categories_link)
        end
      end

      def fetch_categories_from_pagination(pagination_links)
        pagination_links.each do |pagination_link|
          ScrapingJob.fetching_catogary_from_pagination("#{BASE_URL}#{pagination_link[:href]}")
        end
      end

      def fetch_businesses_for_particular_category(category, parent_category = nil)
        base_url = "http://www.vconnect.com/nigeria/categories-from-"
        Delayed::Worker.logger.debug "Category : #{category}"
        Delayed::Worker.logger.debug "Parent Category : #{parent_category}"
        category_links  = search_for_category_links(base_url, category)
        number_of_pages = perform_scrapping(category_links.first[:href], category, parent_category, true)
        Delayed::Worker.logger.debug "Scrapping page 1 of #{number_of_pages}"
        (2..number_of_pages).each do |i|
          Delayed::Worker.logger.debug "Scrapping page #{i} of #{number_of_pages}"
          perform_scrapping(category_links.first[:href]+"-page#{i}", category, parent_category)
        end
      end

      def fetch_special_categories
        SPECIAL_CATEGORIES.each do |parent_category, children_category|
          children_category.each do |category|
            fetch_businesses_for_particular_category(category, parent_category)
          end
        end
      end

      def fetch_specific_categories
        SPECIFIC_CATEGORIES.each do |category|
          fetch_businesses_for_particular_category(category)
        end
      end

      def fetch_specific_category
        # url = "http://www.vconnect.com/solcare-med-consult-garki-abuja_b210232"
        # url = "http://www.vconnect.com/eudokas-place-amuwo_odofin-lagos_b107933"
        url = "http://www.vconnect.com/facial-bar-eti_osa-lagos_b74626"
        category = "Medical"
        ScrapingJob.fetch_product_listing_page(url, category)
      end

      def search_for_category_links(base_url, category)
        document = Nokogiri::HTML(open(base_url + category[0].downcase))
        category_links  = fetch_catogary_products_link_from_pagination_links(document.search(PAGINATION_CSS_PATTERN), category)
        category_links += document.search(CSS_FOR_CAT_PRODUCT_INDEX_PAGE).search("a").select { |u| u.text.strip == category }
        category_links
      end

      def perform_scrapping(url, category, parent_category = nil, first_time = false)
        category_document = load_category_listing_page(url)
        product_links = load_products_links(category_document)
        fetch_products(product_links, (parent_category.nil? ? category : parent_category))
        number_of_pages = calculate_number_of_pages_based_on_last_page_link_or_last_pagination_link(category_document) if first_time
      end

      def calculate_number_of_pages_based_on_last_page_link_or_last_pagination_link(category_document)
        if category_document.search(PAGINATION_LAST_PAGE_PATTERN).empty?
          category_document.search("#ctl00_cphMiddleContainer_PagesDisplayTop a").last.try(:text).try(:strip).to_i 
        else
          category_document.search(PAGINATION_LAST_PAGE_PATTERN).first[:href].match(LAST_LINK_PATTERN)[1].to_i 
        end
      end

      def load_products_links(category_document)
        category_document.search(".cnam a")
      end

      def fetch_products(product_links, category)
        product_links.each do |product_link|
          ScrapingJob.delay.fetch_product_listing_page(product_link[:href], category)
        end
      end

      def load_category_listing_page(url)
        Nokogiri::HTML(open(url))
      end

      def fetch_catogary_products_link_from_pagination_links(pagination_links, category)
        category_link = []
        if pagination_links.length > 1
          pagination_links.shift 
          pagination_links.each do |pagination_link|
            category_link_index_page = Nokogiri::HTML(open(BASE_URL + pagination_link[:href]))
            category_link += category_link_index_page.css(CSS_FOR_CAT_PRODUCT_INDEX_PAGE).search("a").select { |u| u.text.strip == category }
          end
        end
        category_link
      end
    end
  end
end

class ScrapingJob
  CSS_FOR_CAT_PRODUCT_INDEX_PAGE = ".browsecat_wrapll, .browsecat_wraplr"
  PAGINATION_CSS_PATTERN         = "#ctl00_cphMiddleContainer_PagesDisplayTop a"

  def self.fetch_category_products_page(categories_link)
    category_link_index_page = Nokogiri::HTML(open(categories_link))
    pagination_links = category_link_index_page.search(PAGINATION_CSS_PATTERN)
    if (pagination_links)
      pagination_links.shift
      VConnect.fetch_categories_from_pagination(pagination_links)      
    end
    category_product_links  = category_link_index_page.css(CSS_FOR_CAT_PRODUCT_INDEX_PAGE).search("a")
    puts category_product_links.map { |u| [u.text.strip , u[:href]] }
  end

  def self.fetching_catogary_from_pagination(url)
    category_link_index_page = Nokogiri::HTML(open(url))
    category_product_links  = category_link_index_page.css(CSS_FOR_CAT_PRODUCT_INDEX_PAGE).search("a")
    puts category_product_links.map { |u| [u.text.strip , u[:href]] }
  end

  def self.fetch_product_listing_page(listing_link, category_name)
    doc           = Nokogiri::HTML(open(listing_link))
    imp_data      = doc.css(".bizdetailbdwrapl")
    name          = doc.css("#ctl00_cphMiddleContainer_lblCompanyName").text.strip
    description   = doc.css("#ctl00_cphMiddleContainer_lblBusinessDescription").text.strip
    logo_url      = doc.css("#ctl00_cphMiddleContainer_imgphoto").try(:first).try(:attributes).try(:fetch, "src").try(:value)
    address       = imp_data.css("#divTabBusinessDetail div div .bizdetails .bizdetailsr span[itemprop='address']").text.strip
    website       = imp_data.css("#ctl00_cphMiddleContainer_divBusinessWebsite a").empty? ? '' : imp_data.css("#ctl00_cphMiddleContainer_divBusinessWebsite a").first[:href].strip
    email         = imp_data.css("#ctl00_cphMiddleContainer_divBusinessEmail span").text.strip
    category      = imp_data.css("#divTabBusinessDetail .bizdetails .bizdetailsr span[itemprop='category']").text.strip
    services      = imp_data.css("#divTabBusinessDetail .bizdetails .bizdetailsr span[itemprop='service']").text.strip
    contact_number= doc.css('#spanBusinessPhone').search('img').first['src'].strip.match(/\/phone_(\d{2,})/)[1]

    s_name = ServiceType.where(:name => category_name).first

    Delayed::Worker.logger.debug "Category : #{category_name}"
    Delayed::Worker.logger.debug "=="*80
    Delayed::Worker.logger.debug "#{category_name}"
    Delayed::Worker.logger.debug "Business Name        : #{name}"
    Delayed::Worker.logger.debug "Business Description : #{description}"
    Delayed::Worker.logger.debug "Address              : #{address}"
    Delayed::Worker.logger.debug "Website              : #{website}"
    Delayed::Worker.logger.debug "Email                : #{email}"
    Delayed::Worker.logger.debug "Category             : #{category}"
    Delayed::Worker.logger.debug "Services             : #{services}"
    Delayed::Worker.logger.debug "Phone Number         : #{contact_number}"
    Delayed::Worker.logger.debug "Logo Image           : #{logo_url}"
    Delayed::Worker.logger.debug "Service Type id      : #{s_name.id}"
    Delayed::Worker.logger.debug "=="*80
    # Saving all the data into Database....

    service = Service.where("name = ? and address = ? and service_type_id = ?", name.encode("utf-8"), address.encode("utf-8"), s_name.id).first
 
    unless service
      service = Service.new( :name => name, 
                             :description => description,
                             :address => address,
                             :email => email,
                             :website => website,
                             :contact_number => contact_number,
                             :service_type_id => s_name.id
                            )
      service.logo = return_valid_logo(logo_url)
      service.save(:validate => false)
      Delayed::Worker.logger.debug "Service Saved with id: #{service.id}"
    else
      service.logo = return_valid_logo(logo_url) unless service.logo.url == logo_url 
      service.save(:validate => false)
      Delayed::Worker.logger.debug "Duplicate Service Found in DB with id: #{service.id}"
    end
  end

  def self.return_valid_logo(logo_url)
    begin
      open(URI.encode(logo_url)) if logo_url
    rescue OpenURI::HTTPError => e
      Delayed::Worker.logger.debug e.message
      nil
    end
  end
end
