require 'nokogiri'
require 'scrapper/vconnect'
require 'scrapper/genesis'
require 'scrapper/silver_bird'
require 'scrapper/afritickets'

task :scrap => :environment do 
  # VConnect.fetch_all
  # VConnect.fetch_category_business_listing_pages
  VConnect.fetch_specific_categories
  # VConnect.fetch_special_categories
end

task :scrape_special => :environment do 
  VConnect.fetch_special_categories
end

task :scrape_specific => :environment do 
  VConnect.fetch_specific_category
end

task :fetch_events => :environment do 
  Afritickets.fetch_events_from_category
end


task :fetch_movie_details, [:classes] => :environment do |task, args|
  MovieTheatreShow.delete_all
  Movie.now_showing(false).delete_all
  classes = args[:classes].split(" ")
  classes.each do |klass|
    cls = Object.const_get(klass.camelize)
    cls.fetch_all_movies
  end
  Movie.now_showing(true).where('id NOT IN (SELECT DISTINCT(movie_id) FROM movie_theatre_shows)').delete_all
end

task :fetch_all_movies => :environment do
  SilverBird.fetch_all_movies
end

task :scrape => :environment do 
  url = "http://www.vconnect.com/nigeria/categories-from-a"
  doc = Nokogiri::HTML(open(url))
  pagination_links = doc.search("#ctl00_cphMiddleContainer_PagesDisplayTop a")
  pagination_links.shift
  pagination_links.each do |link|
    puts link.keys
    puts link[:href]
  end
end