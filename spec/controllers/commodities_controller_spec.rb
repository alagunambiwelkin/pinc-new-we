require 'spec_helper'

describe CommoditiesController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com")
    @commodity = double(Commodity, :name => "Test Commodity", :id => 1, :address => "Dummy", :type => "Commodity")
  end

  describe "#index" do
    pending "TODO...."
  end

  describe "#create" do 
    def do_create 
      post :create, :commodity => FactoryGirl.attributes_for(:commodity)
    end
    before do 
      Commodity.should_receive(:new).and_return(@commodity)
    end
    it "should save" do
      @commodity.should_receive(:save).and_return(true)
      do_create
      response.status.should eq(200)
      response.body.should eq("{\"message\":\"Item has been listed successfully and would be available shortly\",\"id\":#{@commodity.id}}")
    end
    it "should not save" do
      @commodity.should_receive(:save).and_return(false)
      @commodity.stub_chain([:errors, :full_messages, :join]).and_return("Some Errors") 
      do_create
      response.status.should eq(422)
      response.body.should eq("{\"message\":\"Some Errors\"}")
    end
  end

  describe "#show" do 
    def do_show(id)
      get :show, :id => id
    end
    context "Commodity found" do
      before do
        Commodity.stub_chain(:where, :first).and_return(@commodity)
      end

      it do
        @commodity.should_receive(:to_single_hash).and_return("Single Hash Output")
        do_show(@commodity.id)
        response.status.should eq(200)
        response.body.should eq("Single Hash Output")
      end
    end
    context "Commodity not found" do 
      it do
        do_show(@commodity.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"Commodity Not Found\"}")
      end
    end
  end

  describe "#update" do 
    def do_update(id)
      put :update, :id => id
    end
    context "Commodity not found" do 
      it do 
        do_update(0)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"Commodity Not Found\"}")
      end
    end
    context "Commodity Found" do 
      before do 
        Commodity.stub_chain(:where, :first).and_return(@commodity)
      end 
      it "should update" do
        @commodity.should_receive(:update_attributes).and_return(true)
        do_update(@commodity.id)
        response.status.should eq(200)
        response.body.should eq("{}")
      end
      it "should not update" do
        @commodity.should_receive(:update_attributes).and_return(false)
        @commodity.stub_chain(:errors, :full_messages, :join).and_return("Some Errors")
        do_update(@commodity.id)
        response.status.should eq(422)
        response.body.should eq("{\"message\":\"Some Errors\"}")
      end
    end
  end

  describe "#search" do 
    def do_search(name)
      get :search, :name => name
    end
      
    it do 
      Commodity.should_receive(:where).and_return([@commodity])
      @commodity.should_receive(:to_hash).and_return(FactoryGirl.attributes_for(:commodity))
      do_search("test")
      response.status.should eq(200)
      response.body.should eq("[{\"name\":\"Test Commodity\",\"address\":\"A Lane, B Street\",\"description\":\"Testing Description\",\"contact_number\":\"07123456789\",\"email\":\"commodity@test.com\",\"type\":\"Commodity\",\"currency_id\":1}]")
    end
  end
end

