require 'spec_helper'

describe EventsController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com")
    @event = double(Event, :name => "Test Event", :id => 1, :address => "Dummy", :type => "Event")
  end

  describe "#index" do
    def do_index 
      get :index
    end
    context "User Not Found" do 
      it "should not list out events" do 
        do_index
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "User Found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      it "should render" do 
        @user.should_receive(:created_events).and_return([])
        do_index
        response.status.should eq(200)
        response.body.should eq("[]")
      end
    end
  end

  describe "#create" do 
    def do_create 
      post :create, :event => FactoryGirl.attributes_for(:event)
    end
    context "User not found" do 
      it "should not save" do 
        do_create
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end

    context "User Found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        @user.stub_chain(:created_events, :build).and_return(@event) 
      end
      it "should save" do
        @event.should_receive(:save).and_return(true)
        do_create
        response.status.should eq(200)
        response.body.should eq("{\"message\":\"Event is successfully listed and would be available shortly\",\"id\":#{@event.id}}")
      end
      it "should not save" do
        @event.should_receive(:save).and_return(false)
        @event.stub_chain([:errors, :full_messages, :join]).and_return("Some Errors") 
        do_create
        response.status.should eq(422)
        response.body.should eq("{\"message\":\"Some Errors\"}")
      end
    end
  end

  describe "#show" do 
    def do_show(id)
      get :show, :id => id
    end
    context "event found" do
      before do
        Event.stub_chain(:where, :first).and_return(@event)
      end

      it "should render" do
        @event.should_receive(:to_single_hash).and_return("Single Hash Output")
        do_show(@event.id)
        response.status.should eq(200)
        response.body.should eq("Single Hash Output")
      end
    end
    context "event not found" do 
      it "should render" do
        do_show(@event.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"Event Not Found\"}")
      end
    end
  end

  describe "#update" do 
    def do_update(id)
      put :update, :id => id
    end

    context "user not found" do 
      it do 
        do_update(@event.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key        
      end
      context "Event not found" do 
        it do 
          do_update(0)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Event Not Found\"}")
        end
      end
      context "Event Found" do 
        before do 
          Event.stub_chain(:where, :first).and_return(@event)
        end 
        it "should update" do
          @event.should_receive(:update_attributes).and_return(true)
          do_update(@event.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Event Updated Successfully\"}")
        end
        it "should not update" do
          @event.should_receive(:update_attributes).and_return(false)
          @event.stub_chain(:errors, :full_messages, :join).and_return("Some Errors")
          do_update(@event.id)
          response.status.should eq(422)
          response.body.should eq("{\"message\":\"Some Errors\"}")
        end
      end
    end
  end

  describe "#destroy" do 
    def do_destroy(id)
      delete :destroy, :id => id
    end

    context "user not found" do 
      it do 
        do_destroy(@event.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key        
      end
      context "Event not found" do 
        it do 
          do_destroy(0)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Event Not Found\"}")
        end
      end
      context "Event Found" do 
        before do 
          Event.stub_chain(:where, :first).and_return(@event)
        end 
        it "should destroy" do
          @event.should_receive(:destroy).and_return(true)
          do_destroy(@event.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Event Destroyed Successfully\"}")
        end
        it "should not destroy" do
          @event.should_receive(:destroy).and_return(false)
          @event.stub_chain(:errors, :full_messages, :join).and_return("Some Errors")
          do_destroy(@event.id)
          response.status.should eq(422)
          response.body.should eq("{\"message\":\"Some Errors\"}")
        end
      end
    end
  end

  describe "#search" do 
    pending "TODO..."
  end
end
