require 'spec_helper'

describe FavoritesController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com", :id => 1)
    @service = double(Service, :name => "Test Service", :id => 1)
  end

  describe "#index" do 
    def do_index
      get :index
    end
    context "before filter fails" do 
      it do 
        do_index
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "before filter passes" do
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        User.stub_chain(:where, :first).and_return(@user)
      end 
      it do 
        @user.should_receive(:to_favorites_hash).and_return([])
        do_index
        response.status.should eq(200)
        response.body.should eq("[]")
      end
    end
  end

  describe "#create" do 
    def do_create(id)
      post :create, :service => { :id => id }
    end
    context "user not found" do 
      it do 
        do_create(@service.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      context "service found" do
        before do 
          Service.stub_chain(:where, :first).and_return(@service)
        end
        it "should not save" do 
          @user.stub_chain(:services, :include?).and_return(true)
          @user.stub_chain(:services, :<<).and_return([])
          do_create(@service.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Favorite Already Marked\"}")
        end
        it "should save" do 
          @user.stub_chain(:services, :include?).and_return(false)
          @user.stub_chain(:services, :<<).and_return([])
          do_create(@service.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Favorite Successfully Saved\"}")
        end
      end
      context "service not found" do 
        it "should not save" do 
          do_create(@service.id)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Service Not Found\"}")
        end
      end
    end
  end

  describe "#destroy" do 
    def do_destroy(id)
      delete :destroy, :service => { :id => id }
    end
    context "no user present" do 
      it do 
        do_destroy(@service.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user present" do 
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        User.stub_chain(:where, :first).and_return(@user)
      end
      context "service present" do 
        before do 
          Service.stub_chain(:where, :first).and_return(@service)
        end
        it "should delete" do
          @user.stub_chain(:services, :include?).and_return(true)
          @user.stub_chain(:services, :delete).and_return(true)
          do_destroy(@service.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Favorite Successfully Deleted\"}")
        end
        it "should not delete if not present in services" do 
          @user.stub_chain(:services, :include?).and_return(false)
          @user.stub_chain(:services, :delete).and_return(true)
          do_destroy(@service.id)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Favorite Not Found\"}")
        end
      end
      context "service not present" do
        it "should delete" do
          @user.stub_chain(:services, :include?).and_return(true)
          @user.stub_chain(:services, :delete).and_return(true)
          do_destroy(@service.id)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Service Not Found\"}")
        end
      end
    end
  end
end