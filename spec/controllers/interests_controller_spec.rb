require 'spec_helper'

describe InterestsController do
  
  before do
    @interest = double(Interest, :name => "Testing Interest")
    @user = double(User, :api_key => "abcd", :email => "a@b.com")
  end

  describe "#index" do 
    def do_index
      post :index
    end
    context "user not found" do 
      it do 
        do_index
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      context "service found" do 
        it "should save" do 
          Interest.should_receive(:scoped).and_return([@interest])
          @user.should_receive(:interest_ids).and_return([])
          controller.stub(:generate_and_add_new_keys).and_return("[{\"id\":10,\"name\":\"Testing Interest\",\"is_an_interest\": false \}]")
          do_index
          response.status.should eq(200)
          response.body.should eq("[{\"id\":10,\"name\":\"Testing Interest\",\"is_an_interest\": false }]")
        end
      end
    end
  end
end
