require 'spec_helper'

describe ReviewsController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com", :id => 1)
    @service = double(Service, :name => "Test Service", :id => 1)
    @review = double(Review, :id => 1, :content => "This is the testing content for the review")
  end

  describe "#index" do 
    def do_index
      get :index, :service_id => @service.id
    end

    context "before filter fails" do 
      it do 
        do_index
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"Service Not Found\"}")
      end
    end
    context "before filter passes" do
      before do 
        Service.stub_chain(:where, :first).and_return(@service)
      end 
      it do 
        @service.stub_chain(:reviews, :includes, :order).and_return([])
        @service.should_receive(:average_rating).and_return(0.0)
        do_index
        response.status.should eq(200)
        response.body.should eq("{\"reviews\":[],\"average_service_rating\":0.0}")
      end
    end
  end

  describe "#create" do 
    def do_create(id)
      post :create, :service_id => @service.id
    end
    context "user not found" do 
      it do 
        do_create(@service.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        User.stub_chain(:where, :first).and_return(@user)
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      context "service not found" do 
        it "should not save" do 
          do_create(@service.id)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Service Not Found\"}")
        end
      end
      context "service found" do
        before do 
          Service.stub_chain(:where, :first).and_return(@service)
          @service.stub_chain(:reviews, :build).and_return(@review)
          @review.should_receive(:user=).and_return(@user)
        end
        it "should save" do 
          @review.should_receive(:save).and_return(true)
          do_create(@service.id)
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Successfully Saved Review\",\"id\":#{@service.id}}")
        end
        it "should not save" do 
          @review.should_receive(:save).and_return(false)
          @review.stub_chain(:errors, :full_messages, :join).and_return("Something is Wrong")
          do_create(@service.id)
          response.status.should eq(422)
          response.body.should eq("{\"message\":\"Something is Wrong\"}")
        end
      end
    end
  end

  describe "#show" do 
    def do_show(id, service_id = 0)
      get :show, :service_id => service_id, :id => id 
    end
    context "user not found" do 
      it do 
        do_show(@service.id, @review.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        User.stub_chain(:where, :first).and_return(@user)
      end
      context "service not found" do 
        it do 
          do_show(@review.id)
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Service Not Found\"}")
        end
      end
      context "renders out the review object" do 
        before do 
          Service.stub_chain(:where, :first).and_return(@service)
          Review.stub_chain(:where, :first).and_return(@review)
        end
        it do 
          @review.should_receive(:to_custom_review_hash).and_return("this is the render JSON text")
          do_show(@review.id, @service.id)
          response.status.should eq(200)
          response.body.should eq("this is the render JSON text")
        end
      end
    end
  end
end
