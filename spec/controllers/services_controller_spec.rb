require 'spec_helper'

describe ServicesController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com")
    @service = double(Service, :name => "Test Service", :id => 5, :address => "Dummy")
  end

  describe "#index" do 
    def do_index 
      get :index
    end
    it do 
      do_index
      response.status.should eq(200)
      response.body.should eq("[]")
    end
  end

  describe "#create" do 
    def do_create 
      post :create, :service => FactoryGirl.attributes_for(:service)
    end

    before do 
      ServiceType.stub_chain(:where, :first).and_return(true)
      Service.should_receive(:new).and_return(@service)
    end

    it "should save" do
      @service.should_receive(:save).and_return(true) 
      do_create
      response.status.should eq(200)
      response.body.should eq("{\"message\":\"Service is successfully listed and would be available shortly\",\"id\":#{@service.id}}")
    end
    it "should not save" do
      @service.should_receive(:save).and_return(false)
      @service.stub_chain([:errors, :full_messages, :join]).and_return("Some Errors") 
      do_create
      response.status.should eq(422)
      response.body.should eq("{\"message\":\"Some Errors\"}")
    end
  end

  describe "#show" do 
    def do_show(id)
      get :show, :id => id
    end
    context "before filter passes" do
      before do
        Service.stub_chain(:includes, :where, :first).and_return(@service)
        @service.should_receive(:return_all_images_of_service_including_review_images).and_return([])
      end

      it "should render" do
        @service.should_receive(:to_single_hash).and_return("Single Hash Output") 
        @service.should_receive(:type).and_return("Service")
        do_show(@service.id)
        response.status.should eq(200)
        response.body.should eq("Single Hash Output")
      end
    end
    context "before filter fails" do 
      it "should render" do
        do_show(@service.id)
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"Service Not Found\"}")
      end
    end
  end

  describe "#search" do 
    def do_search(name)
      get :search, :name => name
    end
    context "when no user is logged in" do 
      it do
        Service.stub_chain(:includes, :where).and_return(@service)
        controller.should_receive(:objects_hash).and_return([{:id => @service.id,
        :name => @service.name, :address => @service.address}]) 
        do_search("test")
        response.status.should eq(200)
        response.body.should eq("[{\"id\":5,\"name\":\"Test Service\",\"address\":\"Dummy\"}]")
      end
    end
    context "when user is logged in" do 
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        User.stub_chain(:includes, :where, :first).and_return(@user)
        @user.should_receive(:try).and_return([@service.id])
      end
      
      it do 
        Service.stub_chain(:includes, :where).and_return(@service)
        controller.should_receive(:generate_and_add_new_keys).with(@service, [@service.id], :is_favorite).and_return("[{\"id\":5,\"name\":\"Test Service\",\"address\":\"Dummy\", \"is_favorite\":\"true\"}]")
        do_search("test")
        response.status.should eq(200)
        response.body.should eq("[{\"id\":5,\"name\":\"Test Service\",\"address\":\"Dummy\", \"is_favorite\":\"true\"}]")
      end
    end
  end
end