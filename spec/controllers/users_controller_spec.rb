require 'spec_helper'

describe UsersController do
  before do
    @user = double(User, :api_key => "abcd", :email => "a@b.com")
  end

  describe "#login" do 
    def do_login(hash)
      post :login, :user => hash
    end

    context "when before_filter fails" do
      it do
        do_login({:email => "test@testing.com", :password => "12345678"})
        response.status.should eq(404)
        response.header['Content-Type'].should include 'application/json'
        response.body.should eq("{\"message\":\"Incorrect Email and password combination\"}")
      end
    end

    context "when before_filter passes" do 
      context "successfully authenticated" do 
        before do 
          @user.stub(:password).and_return("12345678")
        end
        it do
          User.stub_chain(:where, :first).and_return(@user)
          @user.should_receive(:authenticate).with(@user.password).and_return(true)
          controller.stub(:create_api_key).and_return(@user.api_key) 
          do_login({:email => @user.email, :password => @user.password })
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Login Successful\",\"api_key\":\"abcd\"}")
        end
      end

      context "unsuccessfully authenticated" do 
        it do
          User.stub_chain(:where, :first).and_return(@user) 
          @user.should_receive(:authenticate).with("").and_return(false)
          do_login({:email => @user.email, :password => ""})
          response.status.should eq(406)
          response.body.should eq("{\"message\":\"Incorrect Email and password combination\"}")
        end
      end
    end
  end

  describe "#create" do 
    def do_create
      post :create
    end
    context "it saves successfully" do
      it do 
        User.should_receive(:new).and_return(@user)
        @user.should_receive(:save).and_return(true)
        do_create
        response.status.should eq(200)
        response.body.should eq("{\"message\":\"Account Successfully Created\",\"api_key\":\"abcd\"}")
      end

      it do 
        User.should_receive(:new).and_return(@user)
        @user.should_receive(:save).and_return(false)
        @user.stub_chain(:errors, :full_messages, :join).and_return("Some Error")
        do_create
        response.status.should eq(422)
        response.body.should eq("{\"message\":\"Some Error\"}")
      end
    end
  end

  describe "#settings" do 
    def do_settings
      get :settings
    end
    describe "before filter fails" do
      it do
        do_settings
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    describe "before filter passes" do
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      it do
        User.stub_chain(:where, :first).and_return(@user)
        @user.should_receive(:to_settings_hash).and_return("User in hash form")
        do_settings
        response.status.should eq(200)
        response.body.should eq("User in hash form")
      end
    end
  end

  describe "#change_password" do 
    def do_change_password(old_password)
      put :change_password, {:user => {:password => "123456", :password_confirmation => "123456"}, :old_password => old_password }
    end
    describe "before_filter fails" do 
      it do
        do_change_password("12345678")
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    describe "before filter passes" do
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
      end
      context "when different password is saved" do 
        before do 
          User.stub_chain(:where, :first).and_return(@user)
          controller.stub(:ensure_passwords_are_different).and_return(true)
          @user.should_receive(:authenticate).and_return(true)
        end
        it "should update password" do
          @user.should_receive(:update_attributes).and_return(true)
          do_change_password("12345678")
          response.status.should eq(200)
          response.body.should eq("{\"message\":\"Successfully Updated Password\"}")
        end

        it "should not update password" do 
          @user.should_receive(:update_attributes).and_return(false)
          @user.stub_chain(:errors, :full_messages, :join).and_return("Some Error")
          do_change_password("12345678")
          response.status.should eq(422)
          response.body.should eq("{\"message\":\"Some Error\"}")
        end
      end
      context "when same password is saved again" do
        before do 
          User.stub_chain(:where, :first).and_return(@user)
        end
        it "should not update password" do
          do_change_password("123456")
          response.status.should eq(406)
          response.body.should eq("{\"message\":\"Old and New Passwords need to be different\"}")
        end
      end
      context "when incorrect password is given" do 
        before do 
          User.stub_chain(:where, :first).and_return(@user)
          @user.should_receive(:authenticate).and_return(false)
        end
        it do 
          do_change_password("1237788")
          response.status.should eq(406)
          response.body.should eq("{\"message\":\"Incorrect Password Supplied\"}")
        end
      end
    end
  end

  describe "#update" do 
    def do_update(params = nil)
      put :update, params
    end
    describe "before_filter fails" do
      it do  
        do_update
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    describe "before_filter passes" do
      before do 
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key 
        User.stub_chain(:where, :first).and_return(@user)
      end
      it "saves successfully" do
        @user.should_receive(:update_attributes).and_return(true)
        do_update
        response.status.should eq(200)
        response.body.should eq("{\"message\":\"Successfully Updated User\"}")
      end
      it "saves unsuccessfully" do
        @user.should_receive(:update_attributes).and_return(false)
        @user.stub_chain(:errors, :full_messages, :join).and_return("Some Error")
        do_update
        response.status.should eq(422)
        response.body.should eq("{\"message\":\"Some Error\"}")
      end
    end
  end
end