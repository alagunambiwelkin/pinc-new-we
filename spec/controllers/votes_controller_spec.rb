require 'spec_helper'

describe VotesController do
  before do
    @user     = double(User, :api_key => "abcd", :email => "a@b.com", :id => 1)
    @review   = double(Review, :id => 1, :content => "This is the testing content for the review")
    @vote   = double(Vote, :id => 1)
  end
  describe "#create" do 
    def do_create(review_id, vote_style)
      post :create, :review_id => review_id, :style => vote_style
    end
    context "user not found" do 
      it do 
        do_create(@review.id, "up")
        response.status.should eq(404)
        response.body.should eq("{\"message\":\"User Not Found\"}")
      end
    end
    context "user found" do 
      before do
        @request.env['HTTP_AUTHORIZATION'] = @user.api_key
        User.stub_chain(:where, :first).and_return(@user)
     end
      context "review not found" do 
        it do 
          do_create(0, "up")
          response.status.should eq(404)
          response.body.should eq("{\"message\":\"Review Not Found\"}")
        end
      end
      context "review found" do 
        before do 
          Review.stub_chain(:where, :first).and_return(@review)
        end
        ["up", "down"].each do |vote_style|
          before do 
            "#{vote_style}vote".classify.constantize.stub(:new).and_return(@vote)
            @vote.stub(:user=).and_return(@user)
            @vote.stub(:review=).and_return(@review)
            @vote.stub_chain(:type, :to_s).and_return("#{vote_style}vote".classify)
          end
          it "should create #{vote_style}vote" do 
            @vote.should_receive(:save).and_return(true)
            do_create(@review.id, vote_style)
            response.status.should eq(200)
            response.body.should eq("{\"message\":\"Successfully Saved #{@vote.type.to_s}\"}")
          end
          it "should not create #{vote_style}vote" do 
            @vote.stub_chain(:errors, :full_messages, :join).and_return("Errors")
            @vote.should_receive(:save).and_return(false)
            do_create(@review.id, vote_style)
            response.status.should eq(422)
            response.body.should eq("{\"message\":\"Errors\"}")
          end
        end
      end
    end
  end
end
