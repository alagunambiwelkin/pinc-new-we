FactoryGirl.define do 
  factory :user do 
    email "aditya@vinsol.com"
    password "12345678"
    password_confirmation "12345678"
    api_key Digest::SHA1.hexdigest("1234564")
    gender 1
  end 
  factory :other_user, :class => User do 
    email "new_email@vinsol.com"
    password "12345678"
    password_confirmation "12345678"
    api_key Digest::SHA1.hexdigest("12345678")
    gender 1
  end
  factory :service do 
    name "Test Service"
    address "A Lane, B Street"
    description "Testing Description"
    contact_number "07123456789"
    email "service@test.com"
    type "Service"
  end
  factory :event do 
    name "Test Event"
    address "A Lane, B Street"
    description "Testing Description"
    contact_number "07123456789"
    email "event@test.com"
    type "Event"
    cost 0.0
    start_time "05/05/1995 04:00"
    end_time "05/05/1995 19:00"
  end
  factory :commodity do 
    name "Test Commodity"
    address "A Lane, B Street"
    description "Testing Description"
    contact_number "07123456789"
    email "commodity@test.com"
    type "Commodity"
    currency_id 1
  end
  factory :product do 
    name "Test Product"
    address "A Lane, B Street"
    description "Testing Description"
    contact_number "07123456789"
    email "service@test.com"
    type "Product"
  end
  factory :interest do 
    name "Testing Interest"
  end
  factory :review do 
    content "This is the dummy text to ensure that the length of the content exceeds 50"
  end
end