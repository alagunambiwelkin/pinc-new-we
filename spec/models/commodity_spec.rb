require "spec_helper"

describe Commodity do
  before(:each) do 
    @commodity = FactoryGirl.create(:commodity)
  end

  describe "Validations" do 
    it {should validate_presence_of(:currency_id)}
    it {should validate_presence_of(:cost)}
  end

  describe "Methods" do
    it { should respond_to(:to_hash)}
    it { should respond_to(:to_single_hash)}
    it "should respond_to #to_hash" do 
      @commodity.to_hash.should eq({"address"=>"A Lane, B Street", "contact_number"=>"07123456789", 
                                    "id"=>@commodity.id, "name"=>"Test Commodity", 
                                    "type"=>"Commodity", :thumb_logo_url=>"/logos/thumb/missing.png", 
                                    :iphone_image_logo_url=>"/logos/iphone/missing.png"
                                  })
    end

    it "should respond_to #to_single_hash" do 
      @commodity.to_single_hash.should eq({ "address"=>"A Lane, B Street", "contact_number"=>"07123456789", 
                                            "cost"=>0.0, "description"=>"Testing Description",
                                            "email"=>"commodity@test.com", 
                                            "id"=>@commodity.id, "latitude"=>nil, "longitude"=>nil, 
                                            "name"=>"Test Commodity", "user_id"=>nil, 
                                            :display_image_logo_url=>"/logos/display/missing.png", 
                                            :iphone_image_logo_url=>"/logos/iphone/missing.png", 
                                            :images=>[]})
    end
  end
end