require "spec_helper"

describe Event do
  before(:each) do 
    @event = FactoryGirl.create(:event)
  end

  describe "Relationships" do 
    it { should belong_to(:user) }
  end

  describe "Validations" do 
    it {should validate_presence_of(:start_time)}
    it {should validate_presence_of(:end_time)}
  end

  describe "Methods" do
    it { should respond_to(:to_hash)}
    it { should respond_to(:to_single_hash)}
    it "should respond_to #to_hash" do 
      @event.to_hash.should eq({  "address"=>"A Lane, B Street", "contact_number"=>"07123456789", 
                                  "cost"=>0.0, "description"=>"Testing Description", 
                                  "email"=>"event@test.com", "id"=>@event.id, 
                                  "name"=>"Test Event", :thumb_logo_url=>"/logos/thumb/missing.png", 
                                  :iphone_image_logo_url=>"/logos/iphone/missing.png"
                                })
    end

    it "should respond_to #to_single_hash" do 
      @event.to_single_hash.should eq({"address"=>"A Lane, B Street", "contact_number"=>"07123456789",
                                       "cost"=>0.0, "description"=>"Testing Description", 
                                       "email"=>"event@test.com", "end_time"=>"Fri, 05 May 1995 19:00:00 UTC +00:00", 
                                       "id"=>@event.id, "latitude"=>nil, "longitude"=>nil, 
                                       "name"=>"Test Event", "reviews_count"=>0, 
                                       "start_time"=>"Fri, 05 May 1995 04:00:00 UTC +00:00", 
                                       "user_id"=>nil, :display_image_logo_url=>"/logos/display/missing.png", 
                                       :iphone_image_logo_url=>"/logos/iphone/missing.png"
                                       })
    end
  end
end