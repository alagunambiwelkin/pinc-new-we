require "spec_helper"

describe Favorite do
  describe "Relationships" do 
    it { should belong_to(:user) }
    it { should belong_to(:favorite_product) } 
  end
  describe "Validations" do 
    it { should validate_uniqueness_of(:favorite_product_id).scoped_to(:user_id) }
  end
end