require 'spec_helper'

describe Image do
  describe "Attr Accessible" do 
    it { should allow_mass_assignment_of(:image) }
  end

  describe "Attachment File" do 
    it { should have_attached_file(:image)}
    it { should validate_attachment_content_type(:image).
                allowing('image/png', 'image/gif', 'image/jpg') }
  end

  describe "Relationships" do 
    it { should belong_to(:imageable) }
  end
end
