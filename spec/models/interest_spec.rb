require 'spec_helper'

describe Interest do
  before(:each) do 
    @interest = FactoryGirl.create(:interest)
  end
  describe "Relationships" do 
    it { should have_many(:users_interests).dependent(:destroy) }
    it { should have_many(:users).through(:users_interests) }
  end
  describe "Validations" do 
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end
  describe "Methods" do
    it { should respond_to(:to_hash) } 
    it "should respond_to #to_hash" do 
      @interest.to_hash.should eq({:id=>@interest.id, :name=> "Testing Interest" })
    end
  end
end
