require 'spec_helper'

describe Product do
  before(:each) do 
    @product = FactoryGirl.create(:product)
  end

  describe "validations" do 
    it { should validate_presence_of(:address) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:contact_number) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:name).scoped_to(:address).with_message("has been registered with same address. Kindly search")}
    it { should ensure_length_of(:description).is_at_most(200) }
    it { should allow_value("12345678901").for(:contact_number) }
    it { should_not allow_value("123456").for(:contact_number) }
    it { should_not allow_value("abc").for(:email)}
    it { should allow_value("a@b.com").for(:email)}
    it { should validate_numericality_of(:contact_number).only_integer }
  end

  describe "Relationships" do 
    it { should belong_to(:currency) }
    it { should have_many(:images).dependent(:destroy) }
    it { should have_attached_file(:logo) }
  end

  describe "Nested Attributes" do 
    it { should accept_nested_attributes_for(:images) }
  end

  describe "Methods" do
    it { should respond_to(:display_image_logo_url) }
    it { should respond_to(:thumb_logo_url) }
    it { should respond_to(:iphone_image_logo_url) }
    it { should respond_to(:logo_url) }
    it { should respond_to(:users) }
    it "should respond_to #display_image_logo_url" do 
      @product.display_image_logo_url.should eq("/logos/display/missing.png")
    end
    it "should respond_to #thumb_logo_url" do 
      @product.thumb_logo_url.should eq("/logos/thumb/missing.png")
    end   
    it "should respond_to #iphone_image_logo_url" do 
      @product.iphone_image_logo_url.should eq("/logos/iphone/missing.png")
    end   
    it "should respond_to #logo_url" do 
      @product.logo_url.should eq("/logos/original/missing.png")
    end   
    it "should respond_to #users" do 
      @product.users.should eq([])
    end
  end
end
