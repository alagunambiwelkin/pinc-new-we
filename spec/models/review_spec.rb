require 'spec_helper'

describe Review do
  describe "Attr Accessible" do 
    it { should allow_mass_assignment_of(:content) }
  end

  describe "Relationships" do 
    it { should belong_to(:user) }
    it { should belong_to(:service) }
    it { should have_many(:votes) }
    it { should have_many(:upvotes) }
    it { should have_many(:downvotes) }
  end

  describe "Validations" do 
    it { should validate_presence_of(:content) }
    it { should ensure_length_of(:content).is_at_least(10)}
  end

  describe "Public Instance Methods" do 
    before do 
      @service = FactoryGirl.create(:service)
      @review = @service.reviews.build(FactoryGirl.attributes_for(:review))
      @review.save
    end
    it "should respond to #to_hash" do 
      @review.to_hash.should eq({ "content"=>"This is the dummy text to ensure that the length of the content exceeds 50", 
                                  "created_at"=>@review.created_at, "downvotes_count"=>0, 
                                  "id"=>@review.id, "rating"=>nil, "upvotes_count"=>0, :images=>[]
                                })
    end
  end
end
