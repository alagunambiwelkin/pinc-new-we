require "spec_helper"

describe Service do
  before(:each) do 
    @service = FactoryGirl.create(:service)
  end

  describe "Relationships" do 
    it { should belong_to(:service_type) }
    it { should have_many(:reviews).dependent(:destroy) }
  end

  describe "Methods" do
    it { should respond_to(:to_hash) }
    it { should respond_to(:to_single_hash) }
    it { should respond_to(:users) }

    it "should respond_to #to_hash" do 
      @service.to_hash.should eq({ "address"=>"A Lane, B Street", "contact_number"=>"07123456789", 
                                   "id"=>@service.id, "name"=>"Test Service", 
                                   :thumb_logo_url=>"/logos/thumb/missing.png", 
                                   :iphone_image_logo_url=>"/logos/iphone/missing.png"})
    end

    it "should respond_to #to_single_hash" do 
      @service.to_single_hash.should eq({ "address"=>"A Lane, B Street", "contact_number"=>"07123456789",
                                          "cost_indicator"=>nil, "description"=>"Testing Description", 
                                          "email"=>"service@test.com", "end_time"=>nil, 
                                          "id"=>@service.id, "latitude"=>nil, 
                                          "longitude"=>nil, "name"=>"Test Service",
                                          "reviews_count"=>0, "start_time"=>nil, 
                                          "user_id"=>nil, :display_image_logo_url=>"/logos/display/missing.png", 
                                          :iphone_image_logo_url=>"/logos/iphone/missing.png", :all_service_images=>[]
                                        })
    end
    it "should respond_to users" do 
      @service.users.should eq([])
    end
  end
end