require 'spec_helper'

describe ServiceType do
  describe "Attr Accessible" do 
    it { should allow_mass_assignment_of(:name) }
  end

  describe "Validations" do 
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end
end
