require "spec_helper"

describe User do
  before(:each) do 
    @user = FactoryGirl.create(:user)
  end

  describe "Relationships" do
    it { should have_many(:favorites).dependent(:destroy) }
    # it { should have_many(:services).through(:favorites) }
    it { should have_many(:users_interests).dependent(:destroy)}
    it { should have_many(:interests).through(:users_interests)}
    it { should have_many(:created_events).class_name("Event")}
  end

  describe "validations" do 
    it { should validate_uniqueness_of(:email) }
    it { should validate_presence_of(:email) }
    it { should allow_value("a@b.com").for(:email) }
    it { should_not allow_value("abc").for(:email) }
    it { should allow_value(nil).for(:api_key) }
    it { should ensure_length_of(:password).is_at_least(5) }
    it { should validate_confirmation_of(:password) }
    it { should allow_value("07899964562").for(:contact_number)}
    it { should allow_value("08899964562").for(:contact_number)}
    # it { should_not allow_value("09899964562").for(:contact_number)}
    it "ensure inclusion of gender" do 
      @user.gender = 4
      @user.should_not be_valid
      @user.errors.full_messages.should eq(["Gender is not included in the list"])
    end
  end

  describe "Callbacks" do 
    it "should respond to before_save" do 
      @user.api_key.should_not be_blank
    end
  end

  describe "Methods" do 
    it { should respond_to(:avatar_url) }
    it { should respond_to(:avatar_thumb_url) }
    it { should respond_to(:avatar_profile_url) }
    it { should respond_to(:to_settings_hash) }
    it "should respond_to to_settings_hash" do 
      @user.to_settings_hash.should eq({ "contact_number"=>nil, 
                                         "date_of_birth"=>nil, "email"=>"aditya@vinsol.com", 
                                         "favorites_count"=>0, "gender"=>1, 
                                         "id"=>@user.id, "location"=>nil, 
                                         "name"=>nil, "position"=>nil, 
                                         :avatar_profile_url=>"/avatars/profile/missing.png", 
                                         :avatar_iphone_image_url=>"/avatars/iphone_image/missing.png"
                                      })
    end
  end
end