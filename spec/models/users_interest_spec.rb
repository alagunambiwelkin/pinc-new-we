require 'spec_helper'

describe UsersInterest do
  describe "Relationships" do 
    it { should belong_to(:user) }
    it { should belong_to(:interest) } 
  end
  describe "Validations" do 
    it { should validate_uniqueness_of(:interest_id).scoped_to(:user_id) }
  end
end
