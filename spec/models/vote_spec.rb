require 'spec_helper'

describe Vote do
  describe "Relationships" do 
    it { should belong_to(:user) }
    it { should belong_to(:review) }
  end
end
